package com.daksa.chandra.controller;

/**
 * Created by User on 9/3/2015.
 */

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daksa.chandra.spmmobile.R;

import java.util.ArrayList;
import java.util.List;

public class LabelsAdapter extends BaseAdapter{
    List<String> param_text = new ArrayList<>();
    List<String> param_color = new ArrayList<>();

    Context context;
    Activity act;

    private static LayoutInflater inflater=null;

    public LabelsAdapter(Activity fragmentTask, List<String> param_text, List<String> param_color) {
        this.param_text = param_text;
        this.param_color = param_color;

        act = fragmentTask;
        context=fragmentTask.getApplicationContext();
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return param_text.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder {
        TextView tv_text;
        LinearLayout layout;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        Holder holder=new Holder();
        final View rowView;
        rowView = inflater.inflate(R.layout.list_labels, null);
        holder.layout = (LinearLayout) rowView.findViewById(R.id.layout_label);
        holder.tv_text=(TextView) rowView.findViewById(R.id.txt_labels);

        holder.tv_text.setText(param_text.get(position));
        holder.layout.setBackgroundColor(Color.parseColor(param_color.get(position)));
        return rowView;
    }
}