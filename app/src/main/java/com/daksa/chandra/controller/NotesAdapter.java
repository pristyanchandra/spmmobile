package com.daksa.chandra.controller;

/**
 * Created by User on 9/3/2015.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daksa.chandra.data.Data;
import com.daksa.chandra.spmmobile.NoteDetail;
import com.daksa.chandra.spmmobile.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class NotesAdapter extends BaseAdapter implements PopupMenu.OnMenuItemClickListener{
    List<String> param_id = new ArrayList<>();
    List<String> param_title = new ArrayList<>();
    List<String> param_content = new ArrayList<>();
    List<String> param_created = new ArrayList<>();
    List<String> param_modified = new ArrayList<>();

    String cek_cut = "";
    String responsemsg = "";
    String parseResponse = "";
    Context context;
    Activity act;
    String id_note = "";
    String msg = "";

    private static LayoutInflater inflater=null;

    public NotesAdapter(Activity fragmentTask, List<String> param_id, List<String> param_title, List<String> param_content, List<String> param_created, List<String> param_modified) {
        // TODO Auto-generated constructor stub
        this.param_id = param_id;
        this.param_title = param_title;
        this.param_content = param_content;
        this.param_created = param_created;
        this.param_modified = param_modified;

        act = fragmentTask;
        context=fragmentTask.getApplicationContext();
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return param_id.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_notes_edit:
                Toast.makeText(context, "Edit", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.item_notes_delete:
                showAlert();
                return true;
        }
        return false;
    }

    public class Holder {
        RelativeLayout layout;
        TextView tv_title;
        TextView tv_created;
        TextView tv_modified;
        ImageButton btn_more;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder=new Holder();
        final View rowView;
        rowView = inflater.inflate(R.layout.list_note, null);
        holder.layout = (RelativeLayout) rowView.findViewById(R.id.layout_notes);
        holder.tv_title =( TextView) rowView.findViewById(R.id.txt_notes_title);
        holder.tv_created = (TextView) rowView.findViewById(R.id.txt_notes_created);
        holder.tv_modified = (TextView) rowView.findViewById(R.id.txt_notes_modified);
        holder.btn_more = (ImageButton) rowView.findViewById(R.id.btn_notes_option);

        holder.tv_title.setText(param_title.get(position));
        holder.tv_created.setText(param_created.get(position));
        holder.tv_modified.setText(param_modified.get(position));
        holder.btn_more.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                id_note = param_id.get(position);
                PopupMenu popupMenu = new PopupMenu(context, v);
                popupMenu.setOnMenuItemClickListener(NotesAdapter.this);
                popupMenu.inflate(R.menu.popup_menu);
                popupMenu.show();
            }
        });
        rowView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    Note.id_note = param_id.get(position);
                    Note.title = param_title.get(position);
                    Note.content = param_content.get(position);
                    Intent i = new Intent(context, NoteDetail.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(i);
                } catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(context, "Error " + e, Toast.LENGTH_LONG).show();
                }
            }
        });




        return rowView;
    }

    void showAlert(){
        AlertDialog.Builder alertdialog = new AlertDialog.Builder(act);
        alertdialog.setTitle("Confirmation");
        alertdialog.setMessage("Are you sure to delete this note?");
        alertdialog.setIcon(R.drawable.ic_warning_black_24dp);
        alertdialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                new DeleteNotes().execute();
            }
        });
        alertdialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertdialog.show();
    }

    private class DeleteNotes extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                URLConnection urlconnection = new URLConnection();
                responsemsg = urlconnection.deleteMethod(Data.uriDeleteNote, id_note);
                JSONObject row = new JSONObject(responsemsg);
                parseResponse = row.getString("msg");

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            act.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(context, parseResponse, Toast.LENGTH_LONG).show();
                }
            });
        }
    }
}