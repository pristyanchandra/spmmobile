package com.daksa.chandra.controller;

/**
 * Created by User on 9/3/2015.
 */
public class DateConversion {
    public String conv(String input){
        String hasil;
        String tanggal;
        String bulan;
        String tahun;
        String [] split = new String[3];
        int i = 0;
        for (String retval: input.split("-")){
            split[i] = retval;
            i++;
        }

        tanggal = split[0];
        bulan = split[1];
        tahun = split[2];

        if (tanggal.length() == 1){
            tanggal = "0"+tanggal;
        }

        switch (bulan) {
            case "1":
                bulan = "Jan";
                break;
            case "2":
                bulan = "Feb";
                break;
            case "3":
                bulan = "Mar";
                break;
            case "4":
                bulan = "Apr";
                break;
            case "5":
                bulan = "May";
                break;
            case "6":
                bulan = "Jun";
                break;
            case "7":
                bulan = "Jul";
                break;
            case "8":
                bulan = "Aug";
                break;
            case "9":
                bulan = "Sep";
                break;
            case "10":
                bulan = "Oct";
                break;
            case "11":
                bulan = "Nov";
                break;
            case "12":
                bulan = "Dec";
                break;
        }

        hasil = tanggal+"-"+bulan+"-"+tahun;
        return hasil;
    }
}
