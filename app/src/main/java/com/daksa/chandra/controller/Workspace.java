package com.daksa.chandra.controller;

import java.util.ArrayList;

/**
 * Created by User on 9/11/2015.
 */
public class Workspace {

    public ArrayList<String> id;
    public ArrayList<String> title;
    public ArrayList<String> project;
    public ArrayList<String> creator;
    public ArrayList<String> created_time;
    public ArrayList<String> progress;
    public ArrayList<String> members;

    public static String id_static = "";
    public static String workspace_name = "";

    public Workspace(){
        id = new ArrayList<>();
        title = new ArrayList<>();
        project = new ArrayList<>();
        creator = new ArrayList<>();
        created_time = new ArrayList<>();
        progress = new ArrayList<>();
        members = new ArrayList<>();
    }


    public ArrayList<String> getId() {
        return id;
    }

    public ArrayList<String> getTitle() {
        return title;
    }

    public ArrayList<String> getProject() {
        return project;
    }

    public ArrayList<String> getCreator() {
        return creator;
    }

    public ArrayList<String> getCreated_time() {
        return created_time;
    }

    public ArrayList<String> getProgress() {
        return progress;
    }

    public ArrayList<String> getMembers() {
        return members;
    }
}
