package com.daksa.chandra.controller;

/**
 * Created by User on 9/3/2015.
 */
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daksa.chandra.data.Data;
import com.daksa.chandra.spmmobile.FragmentDetail;
import com.daksa.chandra.spmmobile.TaskComment;
import com.daksa.chandra.spmmobile.R;
import com.daksa.chandra.spmmobile.TaskAttach;
import com.daksa.chandra.spmmobile.TaskDetail;
import com.wefika.flowlayout.FlowLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class TaskAdapter extends BaseAdapter{
    ArrayList<String> param_title = new ArrayList<>();
    ArrayList<String> param_workspace = new ArrayList<>();
    ArrayList<String> param_due_date = new ArrayList<>();
    ArrayList<String> param_spent = new ArrayList<>();
    ArrayList<String> param_labels = new ArrayList<>();
    ArrayList<String> param_id = new ArrayList<>();
    ArrayList<String> param_status = new ArrayList<>();
    ArrayList<String> param_assignees = new ArrayList<>();
    ArrayList<String> param_description = new ArrayList<>();
    ArrayList<String> param_createdtime = new ArrayList<>();
    ArrayList<String> param_lastmodified = new ArrayList<>();
    ArrayList<String> param_estimation = new ArrayList<>();
    ArrayList<String> param_owner = new ArrayList<>();
    ArrayList<String> label_cut = new ArrayList<>();

    String cek_cut = "";
    String responsemsg = "";
    String parseResponse = "";
    Context context;
    Activity act;
    String id_task = "";
    String status = "";
    String spent = "";

    private static LayoutInflater inflater=null;

    public TaskAdapter(Activity fragmentTask, ArrayList<String> param_id, ArrayList<String> param_title, ArrayList<String> param_workspace,
                       ArrayList<String> param_due_date, ArrayList<String> param_spent, ArrayList<String> param_labels, ArrayList<String> param_status,
                       ArrayList<String> param_assignees, ArrayList<String> param_description, ArrayList<String> param_createdtime,
                       ArrayList<String> param_lastmodified, ArrayList<String> param_estimation, ArrayList<String> param_owner) {
        // TODO Auto-generated constructor stub
        this.param_id = param_id;
        this.param_title = param_title;
        this.param_workspace = param_workspace;
        this.param_due_date = param_due_date;
        this.param_spent = param_spent;
        this.param_labels = param_labels;
        this.param_status = param_status;
        this.param_assignees = param_assignees;
        this.param_description = param_description;
        this.param_createdtime = param_createdtime;
        this.param_lastmodified = param_lastmodified;
        this.param_estimation = param_estimation;
        this.param_owner = param_owner;

        act = fragmentTask;
        context=fragmentTask.getApplicationContext();
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return param_id.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder {
        TextView tv_title;
        TextView tv_workspace;
        TextView tv_due;
        TextView tv_spent;
        TextView tv_status;
        TextView tv_comment;
        TextView tv_attach;
        TextView tv_hours;
        Button btn_lbl_1;
        Button btn_lbl_2;
        Button btn_lbl_3;
        Button btn_lbl_4;
        Button btn_lbl_5;
        Button btn_lbl_6;
        Button btn_lbl_7;
        Button btn_lbl_8;
        Button btn_lbl_9;
        Button btn_lbl_10;
        FlowLayout lay_flow;
        LinearLayout lay_status;
        LinearLayout layout;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder=new Holder();
        final View rowView;
        rowView = inflater.inflate(R.layout.list_task, null);
        holder.layout = (LinearLayout) rowView.findViewById(R.id.layout_task);
        holder.tv_title=(TextView) rowView.findViewById(R.id.tv_task_title);
        holder.tv_workspace=(TextView) rowView.findViewById(R.id.tv_task_workspace);
        holder.tv_due=(TextView) rowView.findViewById(R.id.tv_task_duedate);
        holder.tv_spent=(TextView) rowView.findViewById(R.id.tv_task_spent);
        holder.tv_status=(TextView) rowView.findViewById(R.id.tv_task_status);
        holder.lay_status = (LinearLayout) rowView.findViewById(R.id.lay_satatus);
        holder.tv_comment = (TextView) rowView.findViewById(R.id.tv_task_comment);
        holder.tv_attach = (TextView) rowView.findViewById(R.id.tv_task_atachment);
        holder.tv_hours = (TextView) rowView.findViewById(R.id.tv_task_hours);
        holder.btn_lbl_1 = (Button) rowView.findViewById(R.id.btn_task_label_1);
        holder.btn_lbl_2 = (Button) rowView.findViewById(R.id.btn_task_label_2);
        holder.btn_lbl_3 = (Button) rowView.findViewById(R.id.btn_task_label_3);
        holder.btn_lbl_4 = (Button) rowView.findViewById(R.id.btn_task_label_4);
        holder.btn_lbl_5 = (Button) rowView.findViewById(R.id.btn_task_label_5);
        holder.btn_lbl_6 = (Button) rowView.findViewById(R.id.btn_task_label_6);
        holder.btn_lbl_7 = (Button) rowView.findViewById(R.id.btn_task_label_7);
        holder.btn_lbl_8 = (Button) rowView.findViewById(R.id.btn_task_label_8);
        holder.btn_lbl_9 = (Button) rowView.findViewById(R.id.btn_task_label_9);
        holder.btn_lbl_10 = (Button) rowView.findViewById(R.id.btn_task_label_10);

        holder.btn_lbl_1.setVisibility(View.GONE);
        holder.btn_lbl_2.setVisibility(View.GONE);
        holder.btn_lbl_3.setVisibility(View.GONE);
        holder.btn_lbl_4.setVisibility(View.GONE);
        holder.btn_lbl_5.setVisibility(View.GONE);
        holder.btn_lbl_6.setVisibility(View.GONE);
        holder.btn_lbl_7.setVisibility(View.GONE);
        holder.btn_lbl_8.setVisibility(View.GONE);
        holder.btn_lbl_9.setVisibility(View.GONE);
        holder.btn_lbl_10.setVisibility(View.GONE);

        holder.lay_flow = (FlowLayout) rowView.findViewById(R.id.lay_flow);
        holder.tv_title.setText(param_title.get(position));
        holder.tv_workspace.setText(param_workspace.get(position));
        holder.tv_due.setText(param_due_date.get(position));
        holder.tv_spent.setText(param_spent.get(position));
        holder.tv_status.setText(param_status.get(position));

        holder.tv_spent.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(act);
                dialog.setContentView(R.layout.dialog_spent);
                dialog.setTitle(" Spent Time ");
                dialog.closeOptionsMenu();
                dialog.setCancelable(true);
                id_task = param_id.get(position);
                final EditText txt_spenttime = (EditText) dialog.findViewById(R.id.txt_dialog_spent);
                ImageButton btn_update = (ImageButton) dialog.findViewById(R.id.btn_dialog_spent);
                btn_update.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        spent = txt_spenttime.getText().toString();
                        Toast.makeText(context, spent, Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        holder.tv_hours.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(act);
                dialog.setContentView(R.layout.dialog_spent);
                dialog.setTitle(" Spent Time ");
                dialog.closeOptionsMenu();
                dialog.setCancelable(true);
                id_task = param_id.get(position);
                final EditText txt_spenttime = (EditText) dialog.findViewById(R.id.txt_dialog_spent);
                ImageButton btn_update = (ImageButton) dialog.findViewById(R.id.btn_dialog_spent);
                btn_update.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        spent = txt_spenttime.getText().toString();
                        Toast.makeText(context, spent, Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
        try {
            label_cut.clear();
            splitLabel(cutLabel(param_labels.get(position)));
            clearLabel();
            int i = 0;
            while (i < label_cut.size()){
                switch (i){
                    case 0:
                        holder.btn_lbl_1.setVisibility(View.VISIBLE);
                        holder.btn_lbl_1.setText(label_cut.get(i));
                        holder.btn_lbl_1.setBackgroundResource(R.drawable.back_label);
                        break;
                    case 1:
                        holder.btn_lbl_2.setVisibility(View.VISIBLE);
                        holder.btn_lbl_2.setText(label_cut.get(i));
                        holder.btn_lbl_2.setBackgroundResource(R.drawable.back_label);
                        break;
                    case 2:
                        holder.btn_lbl_3.setVisibility(View.VISIBLE);
                        holder.btn_lbl_3.setText(label_cut.get(i));
                        holder.btn_lbl_3.setBackgroundResource(R.drawable.back_label);
                        break;
                    case 3:
                        holder.btn_lbl_4.setVisibility(View.VISIBLE);
                        holder.btn_lbl_4.setText(label_cut.get(i));
                        holder.btn_lbl_4.setBackgroundResource(R.drawable.back_label);
                        break;
                    case 4:
                        holder.btn_lbl_5.setVisibility(View.VISIBLE);
                        holder.btn_lbl_5.setText(label_cut.get(i));
                        holder.btn_lbl_5.setBackgroundResource(R.drawable.back_label);
                        break;
                    case 5:
                        holder.btn_lbl_6.setVisibility(View.VISIBLE);
                        holder.btn_lbl_6.setText(label_cut.get(i));
                        holder.btn_lbl_6.setBackgroundResource(R.drawable.back_label);
                        break;
                    case 6:
                        holder.btn_lbl_7.setVisibility(View.VISIBLE);
                        holder.btn_lbl_7.setText(label_cut.get(i));
                        holder.btn_lbl_7.setBackgroundResource(R.drawable.back_label);
                        break;
                    case 7:
                        holder.btn_lbl_8.setVisibility(View.VISIBLE);
                        holder.btn_lbl_8.setText(label_cut.get(i));
                        holder.btn_lbl_8.setBackgroundResource(R.drawable.back_label);
                        break;
                    case 8:
                        holder.btn_lbl_9.setVisibility(View.VISIBLE);
                        holder.btn_lbl_9.setText(label_cut.get(i));
                        holder.btn_lbl_9.setBackgroundResource(R.drawable.back_label);
                        break;
                    case 9:
                        holder.btn_lbl_10.setVisibility(View.VISIBLE);
                        holder.btn_lbl_10.setText(label_cut.get(i));
                        holder.btn_lbl_10.setBackgroundResource(R.drawable.back_label);
                        break;

                }
                i++;
            }

        } catch (Exception e){
            e.printStackTrace();
        }

        if ((param_status.get(position)).equals("Completed") || (param_status.get(position)).equals("Reviewed") || (param_status.get(position)).equals("Ignored")){
            holder.lay_status.setBackgroundResource(R.color.completed);
            holder.tv_title.setTypeface(null, Typeface.NORMAL);
            holder.layout.setBackgroundResource(R.color.divider);
        } else {
            holder.lay_status.setBackgroundResource(R.color.other);
        }

        holder.tv_status.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(act);
                dialog.setContentView(R.layout.dialog_status);
                dialog.setTitle(" Change Status ");
                dialog.closeOptionsMenu();
                dialog.setCancelable(true);
                id_task = param_id.get(position);
                TextView btn_new = (TextView) dialog.findViewById(R.id.btn_dialog_new);
                btn_new.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        status = "New";
                        new ChangeStatus().execute();
                        dialog.dismiss();
                    }
                });
                TextView btn_inprogress = (TextView) dialog.findViewById(R.id.btn_dialog_inprogress);
                btn_inprogress.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        status = "In Progress";
                        new ChangeStatus().execute();
                        dialog.dismiss();
                    }
                });
                TextView btn_rejected = (TextView) dialog.findViewById(R.id.btn_dialog_rejected);
                btn_rejected.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        status = "Rejected";
                        new ChangeStatus().execute();
                        dialog.dismiss();
                    }
                });
                TextView btn_completed = (TextView) dialog.findViewById(R.id.btn_dialog_completed);
                btn_completed.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        status = "Completed";
                        new ChangeStatus().execute();
                        dialog.dismiss();
                    }
                });
                TextView btn_reviewed = (TextView) dialog.findViewById(R.id.btn_dialog_reviewed);
                btn_reviewed.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        status = "Reviewed";
                        new ChangeStatus().execute();
                        dialog.dismiss();
                    }
                });
                TextView btn_ignored = (TextView) dialog.findViewById(R.id.btn_dialog_ignored);
                btn_ignored.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        status = "Ignored";
                        new ChangeStatus().execute();
                        dialog.dismiss();
                    }
                });


                dialog.show();
            }
        });

        holder.tv_comment.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Task.task_id = param_id.get(position);
                Intent i = new Intent(context, TaskComment.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }
        });

        holder.tv_attach.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Task.task_id = param_id.get(position);
                Intent i = new Intent(context, TaskAttach.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }
        });

        rowView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Task.setTask(param_id.get(position), param_title.get(position), param_workspace.get(position), param_description.get(position),
                        param_owner.get(position), param_labels.get(position), param_estimation.get(position), param_due_date.get(position),
                        param_spent.get(position), param_createdtime.get(position), param_lastmodified.get(position),
                        param_status.get(position), param_assignees.get(position));
                try{
                    Intent i = new Intent(context, FragmentDetail.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(i);
                } catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(context, "Error " + e, Toast.LENGTH_LONG).show();
                }

            }
        });




        return rowView;
    }

    // remove [...]
    public String cutLabel(String labels){
        String result = "";
        String tmp = "";
        int panjang = labels.length();
        tmp = labels.substring(1, (panjang-1));
        result = tmp;
        return result;
    }

    // split label
    public void splitLabel(String input){
        for (String result: input.split(",")){
            label_cut.add(result);
        }

    }

    // remove ""
    public void clearLabel(){
        int total = label_cut.size();
        int i = 0;
        cek_cut = "";
        String tmp = "";
        while (i < total) {
            try {
                tmp = label_cut.get(i);
                int panjang = tmp.length();
                tmp = tmp.substring(1,(panjang-1));
                label_cut.set(i, tmp);
                if (cek_cut.equals("")){
                    cek_cut = label_cut.get(i);
                } else {
                    cek_cut = cek_cut + " - " +label_cut.get(i);
                }

            } catch(Exception e){
                e.printStackTrace();
                Toast.makeText(context, "Error : " + e, Toast.LENGTH_LONG).show();
            }

            i++;
        }
    }

    private class ChangeStatus extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                JSONObject parameters = new JSONObject();
                parameters.put("id_task", id_task);
                parameters.put("status", status);

                URLConnection urlconnection = new URLConnection();
                responsemsg = urlconnection.putMethod(Data.uriStatus, parameters.toString());
                JSONObject row = new JSONObject(responsemsg);
                parseResponse = row.getString("msg");

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            act.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(context, parseResponse, Toast.LENGTH_LONG).show();
                }
            });
        }
    }
}