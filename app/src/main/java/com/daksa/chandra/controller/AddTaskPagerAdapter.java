package com.daksa.chandra.controller;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.daksa.chandra.spmmobile.FragmentRelatedTask;
import com.daksa.chandra.spmmobile.FragmentTaskAssignees;
import com.daksa.chandra.spmmobile.FragmentTaskInfo;
import com.daksa.chandra.spmmobile.FragmentTaskLabels;

/**
 * Created by User on 9/21/2015.
 */
public class AddTaskPagerAdapter extends FragmentPagerAdapter {

    final int PAGE_COUNT = 4;
    // Tab Titles
        private String tabtitles[] = new String[] { "Task Info", "Labels", "Assignees", "Related Tasks" };
    Context context;

    public AddTaskPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {

            case 0:
                FragmentTaskInfo ftaskinfo = new FragmentTaskInfo();
                return ftaskinfo;
            case 1:
                FragmentTaskLabels flabels = new FragmentTaskLabels();
                return flabels;
            case 2:
                FragmentTaskAssignees fassignees = new FragmentTaskAssignees();
                return fassignees;
            case 3:
                FragmentRelatedTask frelated= new FragmentRelatedTask();
                return frelated;
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabtitles[position];
    }
}
