package com.daksa.chandra.controller;

import com.daksa.chandra.data.Data;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by User on 9/4/2015.
 */
public class URLConnection {

    String responsemsg;
    String response;
    int responsecode;
    String jsonResponse;

    public URLConnection(){
        responsecode = 0;
        responsemsg = "";
        response = "";
        jsonResponse = "";
    }

    public String postMethod(String uri, String param){
        try {
            // Open connection to URL and perform POST request.
            URL url = new URL(uri);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true); // Set Http method to POST
            urlConnection.setChunkedStreamingMode(0); // Use default chunk size
            urlConnection.setRequestMethod("POST");
            urlConnection.getOutputStream().write(param.getBytes());

            responsecode = urlConnection.getResponseCode();
            responsemsg = urlConnection.getResponseMessage();
            response = ""+responsecode+" - "+responsemsg;
            jsonResponse = urlConnection.getInputStream().toString();
            if (responsecode == 200) {

                    BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line+"\n");
                    }
                    br.close();
                jsonResponse = sb.toString();
            }

            urlConnection.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return jsonResponse;
    }

    public String putMethod(String uri, String param){
        try {
            // Open connection to URL and perform POST request.
            URL url = new URL(uri);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true); // Set Http method to POST
            urlConnection.setChunkedStreamingMode(0); // Use default chunk size
            urlConnection.setRequestMethod("PUT");
            urlConnection.getOutputStream().write(param.getBytes());

            responsecode = urlConnection.getResponseCode();
            responsemsg = urlConnection.getResponseMessage();
            response = ""+responsecode+" - "+responsemsg;
            jsonResponse = urlConnection.getInputStream().toString();
            if (responsecode == 200) {

                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line+"\n");
                }
                br.close();
                jsonResponse = sb.toString();
            }

            urlConnection.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return jsonResponse;
    }

    public String deleteMethod(String uri, String id){
        try {
            // Open connection to URL and perform POST request.
            URL url = new URL(uri+id);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true); // Set Http method to POST
            urlConnection.setChunkedStreamingMode(0); // Use default chunk size
            urlConnection.setRequestMethod("DELETE");
            urlConnection.connect();

            responsecode = urlConnection.getResponseCode();
            responsemsg = urlConnection.getResponseMessage();
            response = ""+responsecode+" - "+responsemsg;
            jsonResponse = urlConnection.getInputStream().toString();
            if (responsecode == 200) {

                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line+"\n");
                }
                br.close();
                jsonResponse = sb.toString();
            }

            urlConnection.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return jsonResponse;
    }


    public String getMethod(String uri) {

        BufferedReader reader = null;

        try {
            URL url = new URL(uri);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setReadTimeout(10000);
            con.setConnectTimeout(15000);
            con.setDoInput(true);
            con.connect();
            InputStream is = con.getInputStream();
            reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            String data = null;
            jsonResponse = "";
            while ((data = reader.readLine()) != null) {
                jsonResponse += data + "\n";
            }
            return jsonResponse;

        } catch (Exception e) {
            e.printStackTrace();
            return ""+e;
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }

}
