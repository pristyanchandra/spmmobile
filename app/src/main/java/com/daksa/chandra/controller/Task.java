package com.daksa.chandra.controller;

/**
 * Created by User on 9/3/2015.
 */
public class Task {
    public static String task_id = "";
    public static String title = "";
    public static String workspace = "Spm Mobile";
    public static String description = "";
    public static String labels = "";
    public static String estimation = "";
    public static String due_date = "";
    public static String spent_time = "";
    public static String owner = "";
    public static String date = "";
    public static String time = "";
    public static String created_time = "";
    public static String last_modified = "";
    public static String status = "";
    public static String assignees = "";

    public static void setTask(String task_id, String title, String workspace, String description, String owner, String labels, String estimation,
                               String due_date, String spent_time, String created_time, String last_modified, String status, String assignees){
        Task.task_id = task_id;
        Task.title = title;
        Task.workspace = workspace;
        Task.description = description;
        Task.owner = owner;
        Task.labels = labels;
        Task.estimation = estimation;
        Task.due_date = due_date;
        Task.date = due_date.substring(0,11);
        Task.time = due_date.substring(12,17);
        Task.spent_time = spent_time;
        Task.created_time = created_time;
        Task.last_modified = last_modified;
        Task.status = status;
        Task.assignees = assignees;
    }

    public static void clearTask(){
        task_id = "";
        title = "";
        workspace = "";
        description = "";
        labels = "";
        estimation = "";
        due_date = "";
        spent_time = "";
        owner = "";
        date = "";
        time = "";
        created_time = "";
        last_modified = "";
        status = "";
        assignees = "";
    }


}
