package com.daksa.chandra.controller;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 9/8/2015.
 */
public class SpinnerAdapter {
    List<String> list_label = new ArrayList<String>();

    public SpinnerAdapter(){
        list_label.add("Android");
        list_label.add("User Interface");
        list_label.add("Function");
        list_label.add("API");
    }

    public List<String> getListLabel(){
        return list_label;
    }
}
