package com.daksa.chandra.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 9/3/2015.
 */
public class Data {
    // Task
    public static final String uriTask = "http://192.168.1.203:8080/simhost/rest/spm-mobile/task/";
    public static final String uriTaskWorkspace = "http://192.168.1.203:8080/simhost/rest/spm-mobile/task/workspace/";
    public static final String uriComment = "http://192.168.1.203:8080/simhost/rest/spm-mobile/comment/task/";
    public static final String uriAttach = "http://192.168.1.203:8080/simhost/rest/spm-mobile/attach/task/";
    public static final String uriStatus = "http://192.168.1.203:8080/simhost/rest/spm-mobile/status/task/";
    public static final String uriLog = "http://192.168.1.203:8080/simhost/rest/spm-mobile/log/task/";
    public static final String uriSearchTask = "http://192.168.1.203:8080/simhost/rest/spm-mobile/search/task";

    // Workspace
    public static final String uriWorkspace = "http://192.168.1.203:8080/simhost/rest/spm-mobile/workspace/";
    public static final String uriGetAssignees = "http://192.168.1.203:8080/simhost/rest/spm-mobile/assignees/workspace/";
    public static final String uriattachWorkspace = "http://192.168.1.203:8080/simhost/rest/spm-mobile/attach/workspace/";
    public static final String uriLabelWorkspace = "http://192.168.1.203:8080/simhost/rest/spm-mobile/label/workspace/";
    public static final String uriDeleteNote = "http://192.168.1.203:8080/simhost/rest/spm-mobile/notes/";
//    public static final String uriNotes = "http://192.168.1.203:8080/simhost/rest/spm-mobile/label/workspace/";

    // Employee
    public static final String uriGetAllEmployee = "http://192.168.1.203:8080/simhost/rest/spm-mobile/employee/";

    // Assignees
    public static List<String> list_assignees = new ArrayList<>();
    // Related Task
    public static List<String> list_related = new ArrayList<>();
    // Labels
    public static List<String> list_text = new ArrayList<>();
    public static List<String> list_color = new ArrayList<>();

    // Search Task Data
    public static List<String> searchLabels = new ArrayList<>();
    public static List<String> searchAssignees = new ArrayList<>();;
    public static List<String> searchStatus = new ArrayList<>();;
    public static String searchResult = "";

    public static String srcLabels = "";
    public static String srcAssignees = "";
    public static String srcStatus = "";

    // For Fragment Number
    public static int fNumber = 0;

    // TAG TASK
    public static final String TAG_TASK_ID = "id_task";
    public static final String TAG_TASK = "task";
    public static final String TAG_TASK_TITLE = "title";
    public static final String TAG_TASK_WORKSPACE = "workspace";
    public static final String TAG_TASK_OWNER = "owner";
    public static final String TAG_TASK_DESCRIPTION = "description";
    public static final String TAG_TASK_CREATED = "created_time";
    public static final String TAG_TASK_ESTIMATION = "estimation_time";
    public static final String TAG_TASK_DUE = "due_date";
    public static final String TAG_TASK_SPENT = "spent_time";
    public static final String TAG_TASK_LABELS = "labels";
    public static final String TAG_TASK_MODIFIED = "last_modified";
    public static final String TAG_TASK_STATUS = "status";
    public static final String TAG_TASK_ASSIGNEES = "assignees";
    public static final String TAG_TASK_COMMENT = "comment";
    public static final String TAG_TASK_COMMENT_USER = "user";
    public static final String TAG_TASK_COMMENT_DATE = "comment_date";

    public static ArrayList task = new ArrayList();
    public static ArrayList<String> ttl = new ArrayList<>();
    public static ArrayList<String> work = new ArrayList<>();
    public static ArrayList<String> due = new ArrayList<>();
    public static ArrayList<String> status = new ArrayList<>();

    public static ArrayList workspace = new ArrayList();

    public static void clearAdd(){
        list_assignees = new ArrayList<>();
        list_related = new ArrayList<>();
        list_text = new ArrayList<>();
        list_color = new ArrayList<>();
    }

    public static String getLabel(){
        String result = "";
        int size = list_text.size();
        if (size == 0){
            result = "";
        } else {
            int i = 0;
            while (i < size) {
                if (i == 0){
                    result = list_text.get(i);
                } else {
                    result = result + "," + list_text.get(i);
                }
                i++;
            }
        }
        return result;
    }

    public static String getAsg(){
        String result = "";
        int size = list_assignees.size();
        if (size == 0){
            result = "";
        } else {
            int i = 0;
            while (i < size) {
                if (i == 0){
                    result = list_assignees.get(i);
                } else {
                    result = result + "," + list_assignees.get(i);
                }
                i++;
            }
        }
        return result;
    }

    public static String getRelated(){
        String result = "";
        int size = list_related.size();
        if (size == 0){
            result = "";
        } else {
            int i = 0;
            while (i < size) {
                if (i == 0){
                    result = list_related.get(i);
                } else {
                    result = result + "," + list_related.get(i);
                }
                i++;
            }
        }
        return result;
    }

    public static void tambahLabel(String label){
        searchLabels.add(label);
        srcLabels = "";
        int i = 0;
        while (i < searchLabels.size()){
            if (i == 0){
                srcLabels = searchLabels.get(i);
            } else {
                srcLabels = srcLabels + "," + searchLabels.get(i);
            }
            i++;
        }
    }

    public static void hapusLabel(String label){
        searchLabels.remove(label);
        srcLabels = "";
        int i = 0;
        while (i < searchLabels.size()){
            if (i == 0){
                srcLabels = searchLabels.get(i);
            } else {
                srcLabels = srcLabels + "," + searchLabels.get(i);
            }
            i++;
        }
    }

    public static void tambahAssignees(String asg){
        searchAssignees.add(asg);
        srcAssignees= "";
        int i = 0;
        while (i < searchAssignees.size()){
            if (i == 0){
                srcAssignees = searchAssignees.get(i);
            } else {
                srcAssignees = srcAssignees + "," + searchAssignees.get(i);
            }
            i++;
        }
    }

    public static void hapusAssignees(String asg){
        searchAssignees.remove(asg);
        srcAssignees = "";
        int i = 0;
        while (i < searchAssignees.size()){
            if (i == 0){
                srcAssignees = searchAssignees.get(i);
            } else {
                srcAssignees = srcAssignees + "," + searchAssignees.get(i);
            }
            i++;
        }
    }

    public static void tambahStatus(String status) {
        searchStatus.add(status);
        srcStatus= "";
        int i = 0;
        while (i < searchStatus.size()){
            if (i == 0){
                srcStatus = searchStatus.get(i);
            } else {
                srcStatus = srcStatus + "," + searchStatus.get(i);
            }
            i++;
        }
    }

    public static void hapusStatus(String status) {
        searchStatus.remove(status);
        srcStatus = "";
        int i = 0;
        while (i < searchStatus.size()){
            if (i == 0){
                srcStatus = searchStatus.get(i);
            } else {
                srcStatus = srcStatus + "," + searchStatus.get(i);
            }
            i++;
        }
    }

    public static void clearSearch(){
        searchLabels = new ArrayList<>();
        searchAssignees = new ArrayList<>();;
        searchStatus = new ArrayList<>();;

        srcLabels = "";
        srcAssignees = "";
        srcStatus = "";
    }
}
