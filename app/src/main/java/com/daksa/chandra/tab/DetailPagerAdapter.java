package com.daksa.chandra.tab;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import com.daksa.chandra.spmmobile.FragmentTaskAttachment;
import com.daksa.chandra.spmmobile.FragmentTaskComment;
import com.daksa.chandra.spmmobile.FragmentTaskDetail;
import com.daksa.chandra.spmmobile.FragmentTaskLog;

/**
 * Created by User on 9/21/2015.
 */

public class DetailPagerAdapter extends FragmentStatePagerAdapter {

    CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
    int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created


    // Build a Constructor and assign the passed Values to appropriate values in the class
    public DetailPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb) {
        super(fm);

        this.Titles = mTitles;
        this.NumbOfTabs = mNumbOfTabsumb;

    }

    //This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {

        switch (position) {

            case 0:
                FragmentTaskDetail ftaskdetail = new FragmentTaskDetail();
                return ftaskdetail;
            case 1:
                FragmentTaskAttachment fattach = new FragmentTaskAttachment();
                return fattach;
            case 2:
                FragmentTaskComment fcomment = new FragmentTaskComment();
                return fcomment;
            case 3:
                FragmentTaskLog ftasklog = new FragmentTaskLog();
                return ftasklog;
        }
        return null;
    }

    // This method return the titles for the Tabs in the Tab Strip

    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }

    // This method return the Number of tabs for the tabs Strip

    @Override
    public int getCount() {
        return NumbOfTabs;
    }
}
