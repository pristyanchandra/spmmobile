package com.daksa.chandra.tab;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.daksa.chandra.spmmobile.FragmentTaskAttachment;
import com.daksa.chandra.spmmobile.FragmentTaskComment;
import com.daksa.chandra.spmmobile.FragmentTaskDetail;
import com.daksa.chandra.spmmobile.FragmentTaskLog;
import com.daksa.chandra.spmmobile.FragmentWorkspaceFiles;
import com.daksa.chandra.spmmobile.FragmentWorkspaceNotes;
import com.daksa.chandra.spmmobile.FragmentWorkspaceSettings;
import com.daksa.chandra.spmmobile.FragmentWorkspaceTask;

/**
 * Created by User on 9/21/2015.
 */

public class WorkspacePagerAdapter extends FragmentStatePagerAdapter {

    CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
    int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created


    // Build a Constructor and assign the passed Values to appropriate values in the class
    public WorkspacePagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb) {
        super(fm);

        this.Titles = mTitles;
        this.NumbOfTabs = mNumbOfTabsumb;

    }

    //This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {

        switch (position) {

            case 0:
                FragmentWorkspaceTask ftask = new FragmentWorkspaceTask();
                return ftask;
            case 1:
                FragmentWorkspaceNotes fnotes = new FragmentWorkspaceNotes();
                return fnotes;
            case 2:
                FragmentWorkspaceFiles ffile = new FragmentWorkspaceFiles();
                return ffile;
            case 3:
                FragmentWorkspaceSettings fsetting = new FragmentWorkspaceSettings();
                return fsetting;
        }
        return null;
    }

    // This method return the titles for the Tabs in the Tab Strip

    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }

    // This method return the Number of tabs for the tabs Strip

    @Override
    public int getCount() {
        return NumbOfTabs;
    }
}
