package com.daksa.chandra.tab;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.daksa.chandra.spmmobile.FragmentRelatedTask;
import com.daksa.chandra.spmmobile.FragmentTaskAssignees;
import com.daksa.chandra.spmmobile.FragmentTaskInfo;
import com.daksa.chandra.spmmobile.FragmentTaskLabels;

/**
 * Created by User on 9/21/2015.
 */

public class AddTaskPagerAdapter extends FragmentStatePagerAdapter {

    CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
    int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created


    // Build a Constructor and assign the passed Values to appropriate values in the class
    public AddTaskPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb) {
        super(fm);

        this.Titles = mTitles;
        this.NumbOfTabs = mNumbOfTabsumb;

    }

    //This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {

        switch (position) {

            case 0:
                FragmentTaskInfo ftaskinfo = new FragmentTaskInfo();
                return ftaskinfo;
            case 1:
                FragmentTaskLabels flabels = new FragmentTaskLabels();
                return flabels;
            case 2:
                FragmentTaskAssignees fassignees = new FragmentTaskAssignees();
                return fassignees;
            case 3:
                FragmentRelatedTask frelated= new FragmentRelatedTask();
                return frelated;
        }
        return null;
    }

    // This method return the titles for the Tabs in the Tab Strip

    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }

    // This method return the Number of tabs for the tabs Strip

    @Override
    public int getCount() {
        return NumbOfTabs;
    }
}
