package com.daksa.chandra.spmmobile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;

public class SplashScreen extends Activity {
	private static int SPLASH_TIME_OUT = 3000;
	SQLiteDatabase db;

	String email;
	String username;
	
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		setContentView(R.layout.splashscreen);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

					Intent i = new Intent(SplashScreen.this, MainActivity.class);
					startActivity(i);
					finish();

            }
        }, SPLASH_TIME_OUT);
    }

}
