package com.daksa.chandra.spmmobile;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daksa.chandra.controller.Task;
import com.daksa.chandra.controller.URLConnection;
import com.daksa.chandra.data.Data;

import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by User on 9/14/2015.
 */
public class FragmentTaskDetail extends Fragment {

    TextView txt_title;
    TextView txt_workspace;
    TextView txt_owner;
    TextView txt_description;
    TextView txt_created;
    TextView txt_due;
    TextView txt_estimation;
    TextView txt_spent;
    TextView txt_modified;
    TextView txt_read;
    Button txt_status;

    Button btn_label1;
    Button btn_label2;
    Button btn_label3;
    Button btn_label4;
    Button btn_label5;
    Button btn_label6;
    Button btn_label7;
    Button btn_label8;
    Button btn_label9;
    Button btn_label10;

    Button btn_asg1;
    Button btn_asg2;
    Button btn_asg3;
    Button btn_asg4;
    Button btn_asg5;
    Button btn_asg6;
    Button btn_asg7;
    Button btn_asg8;
    Button btn_asg9;
    Button btn_asg10;
    LinearLayout main_layout;

    Handler h = new Handler();
    Animation slide_down;
    Animation slide_up;
    LayoutAnimationController lac;
    String desc_cut = "";

    ArrayList<String> label_cut = new ArrayList<>();

    String cek_cut = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.frag_detail, container, false);
        Data.fNumber = 1;

        txt_title = (TextView) view.findViewById(R.id.txt_detailtask_title);
        txt_workspace = (TextView) view.findViewById(R.id.txt_detailtask_workspace);
        txt_owner = (TextView) view.findViewById(R.id.txt_detailtask_owner);
        txt_description = (TextView) view.findViewById(R.id.txt_detailtask_description);
        txt_created = (TextView) view.findViewById(R.id.txt_detailtask_created);
        txt_due = (TextView) view.findViewById(R.id.txt_detailtask_duedate);
        txt_estimation = (TextView) view.findViewById(R.id.txt_detailtask_estimation);
        txt_spent = (TextView) view.findViewById(R.id.txt_detailtask_spent);
        txt_modified = (TextView) view.findViewById(R.id.txt_detailtask_modified);
        txt_status = (Button) view.findViewById(R.id.txt_detailtask_status);
        txt_read = (TextView) view.findViewById(R.id.txt_readmore);
        txt_read.setVisibility(View.GONE);

        btn_label1 = (Button) view.findViewById(R.id.btn_detail_label_1);
        btn_label2 = (Button) view.findViewById(R.id.btn_detail_label_2);
        btn_label3 = (Button) view.findViewById(R.id.btn_detail_label_3);
        btn_label4 = (Button) view.findViewById(R.id.btn_detail_label_4);
        btn_label5 = (Button) view.findViewById(R.id.btn_detail_label_5);
        btn_label6 = (Button) view.findViewById(R.id.btn_detail_label_6);
        btn_label7 = (Button) view.findViewById(R.id.btn_detail_label_7);
        btn_label8 = (Button) view.findViewById(R.id.btn_detail_label_8);
        btn_label9 = (Button) view.findViewById(R.id.btn_detail_label_9);
        btn_label10 = (Button) view.findViewById(R.id.btn_detail_label_10);

        btn_asg1 = (Button) view.findViewById(R.id.btn_detail_asg_1);
        btn_asg2 = (Button) view.findViewById(R.id.btn_detail_asg_2);
        btn_asg3 = (Button) view.findViewById(R.id.btn_detail_asg_3);
        btn_asg4 = (Button) view.findViewById(R.id.btn_detail_asg_4);
        btn_asg5 = (Button) view.findViewById(R.id.btn_detail_asg_5);
        btn_asg6 = (Button) view.findViewById(R.id.btn_detail_asg_6);
        btn_asg7 = (Button) view.findViewById(R.id.btn_detail_asg_7);
        btn_asg8 = (Button) view.findViewById(R.id.btn_detail_asg_8);
        btn_asg9 = (Button) view.findViewById(R.id.btn_detail_asg_9);
        btn_asg10 = (Button) view.findViewById(R.id.btn_detail_asg_10);

        return view;
    }

    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        h.removeCallbacksAndMessages(null);
        slide_up = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.slide_up);
        slide_down = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.slide_down);
        txt_read.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_description.setText(Task.description);
                txt_read.setVisibility(View.GONE);
                txt_description.startAnimation(slide_down);
            }
        });

        txt_description.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String des_now = txt_description.getText().toString();
                if (des_now.length() > 200) {
                    txt_description.setText(desc_cut);
                    txt_read.setVisibility(View.VISIBLE);
                    txt_description.startAnimation(slide_up);
                } else {
                    txt_description.setText(Task.description);
                }
            }
        });

        setButtonGone();
        try {
            setLabel();
            setAssginees();
        } catch (Exception e){
            showToast("Error " + e);
        }
        setGUI();
    }

    void setButtonGone(){
        btn_label1.setVisibility(View.GONE);
        btn_label2.setVisibility(View.GONE);
        btn_label3.setVisibility(View.GONE);
        btn_label4.setVisibility(View.GONE);
        btn_label5.setVisibility(View.GONE);
        btn_label6.setVisibility(View.GONE);
        btn_label7.setVisibility(View.GONE);
        btn_label8.setVisibility(View.GONE);
        btn_label9.setVisibility(View.GONE);
        btn_label10.setVisibility(View.GONE);

        btn_asg1.setVisibility(View.GONE);
        btn_asg2.setVisibility(View.GONE);
        btn_asg3.setVisibility(View.GONE);
        btn_asg4.setVisibility(View.GONE);
        btn_asg5.setVisibility(View.GONE);
        btn_asg6.setVisibility(View.GONE);
        btn_asg7.setVisibility(View.GONE);
        btn_asg8.setVisibility(View.GONE);
        btn_asg9.setVisibility(View.GONE);
        btn_asg10.setVisibility(View.GONE);
    }

    void setGUI(){

        txt_title.setText(Task.title);
        txt_workspace.setText(Task.workspace);
        txt_owner.setText(Task.owner);

        txt_created.setText(Task.created_time);
        txt_due.setText(Task.due_date);
        txt_estimation.setText(Task.estimation);
        txt_spent.setText(Task.spent_time);
        txt_modified.setText(Task.last_modified);
        txt_status.setText(Task.status);

        if (Task.status.equals("Completed")){
            txt_status.setBackgroundResource(R.color.completed);
        } else if (Task.status.equals("Rejected")){
            txt_status.setBackgroundResource(R.color.rejected);
        } else {
            txt_status.setBackgroundResource(R.color.other);
        }

        if (Task.description.length() > 200){
            desc_cut = Task.description.substring(0, 200) + " ...";
            txt_description.setText(desc_cut);
            txt_read.setVisibility(View.VISIBLE);
        } else {
            txt_description.setText(Task.description);
        }
    }

    // remove [...]
    public String cutString(String labels){
        String result = "";
        String tmp = "";
        int panjang = labels.length();
        tmp = labels.substring(1, (panjang-1));
        result = tmp;
        return result;
    }

    // split label
    public void splitString(String input){
        for (String result: input.split(",")){
            label_cut.add(result);
        }

    }

    // remove ""
    public void clearString(){
        int total = label_cut.size();
        int i = 0;
        cek_cut = "";
        String tmp = "";
        while (i < total) {
            try {
                tmp = label_cut.get(i);
                int panjang = tmp.length();
                tmp = tmp.substring(1,(panjang-1));
                label_cut.set(i, tmp);
                if (cek_cut.equals("")){
                    cek_cut = label_cut.get(i);
                } else {
                    cek_cut = cek_cut + " - " +label_cut.get(i);
                }
            } catch(Exception e){
                e.printStackTrace();
            }
            i++;
        }
    }

    void setAssginees(){
        label_cut.clear();
        splitString(cutString(Task.assignees));
        clearString();
        int i = 0;
        while (i < label_cut.size()){
            switch (i){
                case 0:
                    btn_asg1.setVisibility(View.VISIBLE);
                    btn_asg1.setText(label_cut.get(i));
                    btn_asg1.setBackgroundResource(R.drawable.back_assignees);
                    break;
                case 1:
                    btn_asg2.setVisibility(View.VISIBLE);
                    btn_asg2.setText(label_cut.get(i));
                    btn_asg2.setBackgroundResource(R.drawable.back_assignees);
                    break;
                case 2:
                    btn_asg3.setVisibility(View.VISIBLE);
                    btn_asg3.setText(label_cut.get(i));
                    btn_asg3.setBackgroundResource(R.drawable.back_assignees);
                    break;
                case 3:
                    btn_asg4.setVisibility(View.VISIBLE);
                    btn_asg4.setText(label_cut.get(i));
                    btn_asg4.setBackgroundResource(R.drawable.back_assignees);
                    break;
                case 4:
                    btn_asg5.setVisibility(View.VISIBLE);
                    btn_asg5.setText(label_cut.get(i));
                    btn_asg5.setBackgroundResource(R.drawable.back_assignees);
                    break;
                case 5:
                    btn_asg6.setVisibility(View.VISIBLE);
                    btn_asg6.setText(label_cut.get(i));
                    btn_asg6.setBackgroundResource(R.drawable.back_assignees);
                    break;
                case 6:
                    btn_asg7.setVisibility(View.VISIBLE);
                    btn_asg7.setText(label_cut.get(i));
                    btn_asg7.setBackgroundResource(R.drawable.back_assignees);
                    break;
                case 7:
                    btn_asg8.setVisibility(View.VISIBLE);
                    btn_asg8.setText(label_cut.get(i));
                    btn_asg8.setBackgroundResource(R.drawable.back_assignees);
                    break;
                case 8:
                    btn_asg9.setVisibility(View.VISIBLE);
                    btn_asg9.setText(label_cut.get(i));
                    btn_asg9.setBackgroundResource(R.drawable.back_assignees);
                    break;
                case 9:
                    btn_asg10.setVisibility(View.VISIBLE);
                    btn_asg10.setText(label_cut.get(i));
                    btn_asg10.setBackgroundResource(R.drawable.back_assignees);
                    break;
            }
            i++;
        }
    }

    void setLabel(){
        label_cut.clear();
        splitString(cutString(Task.labels));
        clearString();
        int i = 0;
        while (i < label_cut.size()){
            switch (i){
                case 0:
                    btn_label1.setVisibility(View.VISIBLE);
                    btn_label1.setText(label_cut.get(i));
                    btn_label1.setBackgroundResource(R.drawable.back_label);
                    if (label_cut.get(i).equals("Bug") || label_cut.get(i).equals("bug") || label_cut.get(i).equals("BUG")){
                        btn_label1.setBackgroundResource(R.drawable.back_label_red);
                    }
                    break;
                case 1:
                    btn_label2.setVisibility(View.VISIBLE);
                    btn_label2.setText(label_cut.get(i));
                    btn_label2.setBackgroundResource(R.drawable.back_label);
                    if (label_cut.get(i).equals("Bug") || label_cut.get(i).equals("bug") || label_cut.get(i).equals("BUG")){
                        btn_label2.setBackgroundResource(R.drawable.back_label_red);
                    }
                    break;
                case 2:
                    btn_label3.setVisibility(View.VISIBLE);
                    btn_label3.setText(label_cut.get(i));
                    btn_label3.setBackgroundResource(R.drawable.back_label);
                    if (label_cut.get(i).equals("Bug") || label_cut.get(i).equals("bug") || label_cut.get(i).equals("BUG")){
                        btn_label3.setBackgroundResource(R.drawable.back_label_red);
                    }
                    break;
                case 3:
                    btn_label4.setVisibility(View.VISIBLE);
                    btn_label4.setText(label_cut.get(i));
                    btn_label4.setBackgroundResource(R.drawable.back_label);
                    if (label_cut.get(i).equals("Bug") || label_cut.get(i).equals("bug") || label_cut.get(i).equals("BUG")){
                        btn_label4.setBackgroundResource(R.drawable.back_label_red);
                    }
                    break;
                case 4:
                    btn_label5.setVisibility(View.VISIBLE);
                    btn_label5.setText(label_cut.get(i));
                    btn_label5.setBackgroundResource(R.drawable.back_label);
                    if (label_cut.get(i).equals("Bug") || label_cut.get(i).equals("bug") || label_cut.get(i).equals("BUG")){
                        btn_label5.setBackgroundResource(R.drawable.back_label_red);
                    }
                    break;
                case 5:
                    btn_label6.setVisibility(View.VISIBLE);
                    btn_label6.setText(label_cut.get(i));
                    btn_label6.setBackgroundResource(R.drawable.back_label);
                    if (label_cut.get(i).equals("Bug") || label_cut.get(i).equals("bug") || label_cut.get(i).equals("BUG")){
                        btn_label6.setBackgroundResource(R.drawable.back_label_red);
                    }
                    break;
                case 6:
                    btn_label7.setVisibility(View.VISIBLE);
                    btn_label7.setText(label_cut.get(i));
                    btn_label7.setBackgroundResource(R.drawable.back_label);
                    if (label_cut.get(i).equals("Bug") || label_cut.get(i).equals("bug") || label_cut.get(i).equals("BUG")){
                        btn_label7.setBackgroundResource(R.drawable.back_label_red);
                    }
                    break;
                case 7:
                    btn_label8.setVisibility(View.VISIBLE);
                    btn_label8.setText(label_cut.get(i));
                    btn_label8.setBackgroundResource(R.drawable.back_label);
                    if (label_cut.get(i).equals("Bug") || label_cut.get(i).equals("bug") || label_cut.get(i).equals("BUG")){
                        btn_label8.setBackgroundResource(R.drawable.back_label_red);
                    }
                    break;
                case 8:
                    btn_label9.setVisibility(View.VISIBLE);
                    btn_label9.setText(label_cut.get(i));
                    btn_label9.setBackgroundResource(R.drawable.back_label);
                    if (label_cut.get(i).equals("Bug") || label_cut.get(i).equals("bug") || label_cut.get(i).equals("BUG")){
                        btn_label9.setBackgroundResource(R.drawable.back_label_red);
                    }
                    break;
                case 9:
                    btn_label10.setVisibility(View.VISIBLE);
                    btn_label10.setText(label_cut.get(i));
                    btn_label10.setBackgroundResource(R.drawable.back_label);
                    if (label_cut.get(i).equals("Bug") || label_cut.get(i).equals("bug") || label_cut.get(i).equals("BUG")){
                        btn_label10.setBackgroundResource(R.drawable.back_label_red);
                    }
                    break;

            }
            i++;
        }
    }

    void showToast(String msg){
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

}
