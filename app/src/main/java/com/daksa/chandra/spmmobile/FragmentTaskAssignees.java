package com.daksa.chandra.spmmobile;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.daksa.chandra.controller.URLConnection;
import com.daksa.chandra.data.Data;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 9/21/2015.
 */
public class FragmentTaskAssignees extends Fragment {

    Handler h = new Handler();
    AppCompatAutoCompleteTextView txt_assignee;
    ImageButton btn_assignee;
    ListView listview;

    String employee_response;
    ArrayAdapter<String> employee_adapter;
    List<String> employee_list = new ArrayList<String>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_assignees, container, false);
        txt_assignee = (AppCompatAutoCompleteTextView) view.findViewById(R.id.txt_addtask_assignees);
        listview = (ListView) view.findViewById(R.id.lv_assignees);
        btn_assignee = (ImageButton) view.findViewById(R.id.btn_addtask_assignees);
        Data.fNumber = 1;
        return view;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        h.removeCallbacksAndMessages(null);
        new GetEmployee().execute();
        setListview();
        btn_assignee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Data.list_assignees.add(txt_assignee.getText().toString());
                setListview();
                txt_assignee.setText("");
            }
        });
    }

    void setListview(){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.list_assignees, R.id.txt_assignee, Data.list_assignees);
        listview.setAdapter(adapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                removeList(position);
            }
        });

    }

    void removeList(int position){
        Data.list_assignees.remove(position);
        setListview();
    }

    void showToast(String msg){
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    private class GetEmployee extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                URLConnection urlconnection = new URLConnection();
                employee_response = urlconnection.getMethod(Data.uriGetAssignees+"1");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject obj = new JSONObject(employee_response);
                        JSONArray arr = (JSONArray) obj.getJSONArray("employee");
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject row = arr.getJSONObject(i);
                            employee_list.add(row.getString("employee_name"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    employee_adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, employee_list);
                    txt_assignee.setAdapter(employee_adapter);
                    txt_assignee.setThreshold(1);
                }
            });
        }
    }

}
