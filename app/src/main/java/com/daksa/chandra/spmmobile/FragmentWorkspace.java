package com.daksa.chandra.spmmobile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.daksa.chandra.controller.Task;
import com.daksa.chandra.controller.URLConnection;
import com.daksa.chandra.controller.Workspace;
import com.daksa.chandra.data.Data;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Pristyan on 8/25/2015.
 */
public class FragmentWorkspace extends Fragment {

    ImageButton btn_tambah;
    ListView listview;
    private ProgressDialog pDialog;
    Handler h = new Handler();
    String responsemsg;

    String title = "";
    String project = "";
    String creator = "";
    String created_time = "";
    String progress;
    ArrayList<HashMap<String, String>> workspacelist = new ArrayList<>();
    LayoutAnimationController lac;
    Workspace wks = new Workspace();

    public FragmentWorkspace() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_workspace, container, false);
        Data.fNumber = 0;
        listview = (ListView) view.findViewById(R.id.list_workspace);
        btn_tambah = (ImageButton) view.findViewById(R.id.btn_tambah_workspace);
        btn_tambah.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(getActivity(), TaskDetail.class);
//                startActivity(intent);
            }
        });
        return view;
    }

    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        h.removeCallbacksAndMessages(null);
        try{

            new GetWorkspace().execute();
        } catch (Exception e) {
            showToast(""+e);
        }

    }

    void showToast(String msg){
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    private class GetWorkspace extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Retrieving Data ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {
            URLConnection urlconn = new URLConnection();
            responsemsg = urlconn.getMethod(Data.uriWorkspace);
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject obj = new JSONObject(responsemsg);
                        JSONArray arr = (JSONArray) obj.getJSONArray("workspace");

                        for (int i = 0; i < arr.length(); i++){
                            JSONObject row = arr.getJSONObject(i);
                            title = row.getString("title");
                            project = row.getString("project");
                            creator  = row.getString("creator");
                            created_time = row.getString("created_time");
                            progress = row.getString("progress");

                            wks.id.add(row.getString("id_workspace"));
                            wks.title.add(title);
                            wks.project.add(project);
                            wks.progress.add(progress);
                            wks.created_time.add(created_time);
                            wks.creator.add(creator);

                            HashMap<String, String> map = new HashMap<>();
                            map.put("title", title);
                            map.put("project", project);
                            map.put("creator", creator);
                            map.put("created_time", created_time);
                            map.put("progress", progress);
                            workspacelist.add(map);

                            ListAdapter adapter = new SimpleAdapter(getActivity(), workspacelist, R.layout.list_workspace,
                                    new String[]{"title","project","creator","created_time","progress"},
                                    new int[]{R.id.tv_wks_title, R.id.tv_wks_project, R.id.tv_wks_creator, R.id.tv_wks_createdtime, R.id.tv_wks_progress});
                            listview.setAdapter(adapter);
                            lac = new LayoutAnimationController(AnimationUtils.loadAnimation(getActivity(), R.anim.zoom_in), 0.5f); //0.5f == time between appearance of listview items.
                            listview.setLayoutAnimation(lac);
                            listview.startLayoutAnimation();

                            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    Workspace.id_static = wks.id.get(position);
                                    Workspace.workspace_name = wks.title.get(position);
//                                    showToast(Workspace.id_static);
                                    Intent i = new Intent(getActivity(), FragmentWorkspaceDetail.class);
                                    startActivity(i);

                                }
                            });
                        }

                    } catch (Exception e) {
                        showToast(""+e);
                        Log.e("ERROR", "run ", e);
                    }
//                    showToast(responsemsg);
                }
            });
        }
    }


}
