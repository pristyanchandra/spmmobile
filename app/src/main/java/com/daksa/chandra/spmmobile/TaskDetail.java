package com.daksa.chandra.spmmobile;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daksa.chandra.controller.Task;
import com.daksa.chandra.controller.URLConnection;
import com.daksa.chandra.data.Data;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by User on 9/2/2015.
 */
public class TaskDetail extends ActionBarActivity {

    private Toolbar topToolBar;
    TextView txt_title;
    TextView txt_workspace;
    TextView txt_owner;
    TextView txt_description;
    TextView txt_created;
    TextView txt_due;
    TextView txt_estimation;
    TextView txt_spent;
    TextView txt_modified;
    TextView txt_read;
    Button txt_status;

    Button btn_label1;
    Button btn_label2;
    Button btn_label3;
    Button btn_label4;
    Button btn_label5;
    Button btn_label6;
    Button btn_label7;
    Button btn_label8;
    Button btn_label9;
    Button btn_label10;

    Button btn_asg1;
    Button btn_asg2;
    Button btn_asg3;
    Button btn_asg4;
    Button btn_asg5;
    Button btn_asg6;
    Button btn_asg7;
    Button btn_asg8;
    Button btn_asg9;
    Button btn_asg10;
    LinearLayout main_layout;

    private ProgressDialog pDialog;

    ArrayList<String> label_cut = new ArrayList<>();

    String cek_cut = "";
    String desc_cut = "";

    String responsemsg = "";
    String parseResponse = "";

    Animation slide_down;
    Animation slide_up;
    LayoutAnimationController lac;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.task_detail);
        topToolBar = (Toolbar)findViewById(R.id.toolbar_taskdetail);
        setSupportActionBar(topToolBar);
        getSupportActionBar().setTitle(R.string.task_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        main_layout = (LinearLayout) findViewById(R.id.main_layout_detail);
        lac = new LayoutAnimationController(AnimationUtils.loadAnimation(TaskDetail.this, R.anim.zoom_in), 0.5f); //0.5f == time between appearance of listview items.
        main_layout.setLayoutAnimation(lac);
        main_layout.startLayoutAnimation();

        slide_up = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
        slide_down = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);
        Data.fNumber = 1;

        txt_title = (TextView) findViewById(R.id.txt_detailtask_title);
        txt_workspace = (TextView) findViewById(R.id.txt_detailtask_workspace);
        txt_owner = (TextView) findViewById(R.id.txt_detailtask_owner);
        txt_description = (TextView) findViewById(R.id.txt_detailtask_description);
        txt_created = (TextView) findViewById(R.id.txt_detailtask_created);
        txt_due = (TextView) findViewById(R.id.txt_detailtask_duedate);
        txt_estimation = (TextView) findViewById(R.id.txt_detailtask_estimation);
        txt_spent = (TextView) findViewById(R.id.txt_detailtask_spent);
        txt_modified = (TextView) findViewById(R.id.txt_detailtask_modified);
        txt_status = (Button) findViewById(R.id.txt_detailtask_status);
        txt_read = (TextView) findViewById(R.id.txt_readmore);
        txt_read.setVisibility(View.GONE);
        txt_read.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_description.setText(Task.description);
                txt_read.setVisibility(View.GONE);
                txt_description.startAnimation(slide_down);
            }
        });

        txt_description.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String des_now = txt_description.getText().toString();
                if (des_now.length() > 200) {
                    txt_description.setText(desc_cut);
                    txt_read.setVisibility(View.VISIBLE);
                    txt_description.startAnimation(slide_up);
                } else {
                    txt_description.setText(Task.description);
                }
            }
        });

        btn_label1 = (Button) findViewById(R.id.btn_detail_label_1);
        btn_label2 = (Button) findViewById(R.id.btn_detail_label_2);
        btn_label3 = (Button) findViewById(R.id.btn_detail_label_3);
        btn_label4 = (Button) findViewById(R.id.btn_detail_label_4);
        btn_label5 = (Button) findViewById(R.id.btn_detail_label_5);
        btn_label6 = (Button) findViewById(R.id.btn_detail_label_6);
        btn_label7 = (Button) findViewById(R.id.btn_detail_label_7);
        btn_label8 = (Button) findViewById(R.id.btn_detail_label_8);
        btn_label9 = (Button) findViewById(R.id.btn_detail_label_9);
        btn_label10 = (Button) findViewById(R.id.btn_detail_label_10);

        btn_asg1 = (Button) findViewById(R.id.btn_detail_asg_1);
        btn_asg2 = (Button) findViewById(R.id.btn_detail_asg_2);
        btn_asg3 = (Button) findViewById(R.id.btn_detail_asg_3);
        btn_asg4 = (Button) findViewById(R.id.btn_detail_asg_4);
        btn_asg5 = (Button) findViewById(R.id.btn_detail_asg_5);
        btn_asg6 = (Button) findViewById(R.id.btn_detail_asg_6);
        btn_asg7 = (Button) findViewById(R.id.btn_detail_asg_7);
        btn_asg8 = (Button) findViewById(R.id.btn_detail_asg_8);
        btn_asg9 = (Button) findViewById(R.id.btn_detail_asg_9);
        btn_asg10 = (Button) findViewById(R.id.btn_detail_asg_10);

        setButtonGone();
        try {
            setLabel();
            setAssginees();
        } catch (Exception e){
            showToast("Error " + e);
        }
        setGUI();
    }

    void showAlert(){
        AlertDialog.Builder alertdialog = new AlertDialog.Builder(TaskDetail.this);
        alertdialog.setTitle("Confirmation");
        alertdialog.setMessage("Are you sure to delete this task?");
        alertdialog.setIcon(R.drawable.ic_warning_black_24dp);
        alertdialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteTask();
            }
        });
        alertdialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertdialog.show();
    }

    void setButtonGone(){
        btn_label1.setVisibility(View.GONE);
        btn_label2.setVisibility(View.GONE);
        btn_label3.setVisibility(View.GONE);
        btn_label4.setVisibility(View.GONE);
        btn_label5.setVisibility(View.GONE);
        btn_label6.setVisibility(View.GONE);
        btn_label7.setVisibility(View.GONE);
        btn_label8.setVisibility(View.GONE);
        btn_label9.setVisibility(View.GONE);
        btn_label10.setVisibility(View.GONE);

        btn_asg1.setVisibility(View.GONE);
        btn_asg2.setVisibility(View.GONE);
        btn_asg3.setVisibility(View.GONE);
        btn_asg4.setVisibility(View.GONE);
        btn_asg5.setVisibility(View.GONE);
        btn_asg6.setVisibility(View.GONE);
        btn_asg7.setVisibility(View.GONE);
        btn_asg8.setVisibility(View.GONE);
        btn_asg9.setVisibility(View.GONE);
        btn_asg10.setVisibility(View.GONE);
    }

    void setGUI(){

        txt_title.setText(Task.title);
        txt_workspace.setText(Task.workspace);
        txt_owner.setText(Task.owner);

        txt_created.setText(Task.created_time);
        txt_due.setText(Task.due_date);
        txt_estimation.setText(Task.estimation);
        txt_spent.setText(Task.spent_time);
        txt_modified.setText(Task.last_modified);
        txt_status.setText(Task.status);

        if (Task.status.equals("Completed")){
            txt_status.setBackgroundResource(R.color.completed);
        } else if (Task.status.equals("Rejected")){
            txt_status.setBackgroundResource(R.color.rejected);
        } else {
            txt_status.setBackgroundResource(R.color.other);
        }

        if (Task.description.length() > 200){
            desc_cut = Task.description.substring(0, 200) + " ...";
            txt_description.setText(desc_cut);
            txt_read.setVisibility(View.VISIBLE);
        } else {
            txt_description.setText(Task.description);
        }
    }

    // remove [...]
    public String cutString(String labels){
        String result = "";
        String tmp = "";
        int panjang = labels.length();
        tmp = labels.substring(1, (panjang-1));
        result = tmp;
        return result;
    }

    // split label
    public void splitString(String input){
        for (String result: input.split(",")){
            label_cut.add(result);
        }

    }

    // remove ""
    public void clearString(){
        int total = label_cut.size();
        int i = 0;
        cek_cut = "";
        String tmp = "";
        while (i < total) {
            try {
                tmp = label_cut.get(i);
                int panjang = tmp.length();
                tmp = tmp.substring(1,(panjang-1));
                label_cut.set(i, tmp);
                if (cek_cut.equals("")){
                    cek_cut = label_cut.get(i);
                } else {
                    cek_cut = cek_cut + " - " +label_cut.get(i);
                }
            } catch(Exception e){
                e.printStackTrace();
            }
            i++;
        }
    }

    void setAssginees(){
        label_cut.clear();
        splitString(cutString(Task.assignees));
        clearString();
        int i = 0;
        while (i < label_cut.size()){
            switch (i){
                case 0:
                    btn_asg1.setVisibility(View.VISIBLE);
                    btn_asg1.setText(label_cut.get(i));
                    btn_asg1.setBackgroundResource(R.drawable.back_assignees);
                    break;
                case 1:
                    btn_asg2.setVisibility(View.VISIBLE);
                    btn_asg2.setText(label_cut.get(i));
                    btn_asg2.setBackgroundResource(R.drawable.back_assignees);
                    break;
                case 2:
                    btn_asg3.setVisibility(View.VISIBLE);
                    btn_asg3.setText(label_cut.get(i));
                    btn_asg3.setBackgroundResource(R.drawable.back_assignees);
                    break;
                case 3:
                    btn_asg4.setVisibility(View.VISIBLE);
                    btn_asg4.setText(label_cut.get(i));
                    btn_asg4.setBackgroundResource(R.drawable.back_assignees);
                    break;
                case 4:
                    btn_asg5.setVisibility(View.VISIBLE);
                    btn_asg5.setText(label_cut.get(i));
                    btn_asg5.setBackgroundResource(R.drawable.back_assignees);
                    break;
                case 5:
                    btn_asg6.setVisibility(View.VISIBLE);
                    btn_asg6.setText(label_cut.get(i));
                    btn_asg6.setBackgroundResource(R.drawable.back_assignees);
                    break;
                case 6:
                    btn_asg7.setVisibility(View.VISIBLE);
                    btn_asg7.setText(label_cut.get(i));
                    btn_asg7.setBackgroundResource(R.drawable.back_assignees);
                    break;
                case 7:
                    btn_asg8.setVisibility(View.VISIBLE);
                    btn_asg8.setText(label_cut.get(i));
                    btn_asg8.setBackgroundResource(R.drawable.back_assignees);
                    break;
                case 8:
                    btn_asg9.setVisibility(View.VISIBLE);
                    btn_asg9.setText(label_cut.get(i));
                    btn_asg9.setBackgroundResource(R.drawable.back_assignees);
                    break;
                case 9:
                    btn_asg10.setVisibility(View.VISIBLE);
                    btn_asg10.setText(label_cut.get(i));
                    btn_asg10.setBackgroundResource(R.drawable.back_assignees);
                    break;
            }
            i++;
        }
    }

    void setLabel(){
        label_cut.clear();
        splitString(cutString(Task.labels));
        clearString();
        int i = 0;
        while (i < label_cut.size()){
            switch (i){
                case 0:
                    btn_label1.setVisibility(View.VISIBLE);
                    btn_label1.setText(label_cut.get(i));
                    btn_label1.setBackgroundResource(R.drawable.back_label);
                    if (label_cut.get(i).equals("Bug") || label_cut.get(i).equals("bug") || label_cut.get(i).equals("BUG")){
                        btn_label1.setBackgroundResource(R.drawable.back_label_red);
                    }
                    break;
                case 1:
                    btn_label2.setVisibility(View.VISIBLE);
                    btn_label2.setText(label_cut.get(i));
                    btn_label2.setBackgroundResource(R.drawable.back_label);
                    if (label_cut.get(i).equals("Bug") || label_cut.get(i).equals("bug") || label_cut.get(i).equals("BUG")){
                        btn_label2.setBackgroundResource(R.drawable.back_label_red);
                    }
                    break;
                case 2:
                    btn_label3.setVisibility(View.VISIBLE);
                    btn_label3.setText(label_cut.get(i));
                    btn_label3.setBackgroundResource(R.drawable.back_label);
                    if (label_cut.get(i).equals("Bug") || label_cut.get(i).equals("bug") || label_cut.get(i).equals("BUG")){
                        btn_label3.setBackgroundResource(R.drawable.back_label_red);
                    }
                    break;
                case 3:
                    btn_label4.setVisibility(View.VISIBLE);
                    btn_label4.setText(label_cut.get(i));
                    btn_label4.setBackgroundResource(R.drawable.back_label);
                    if (label_cut.get(i).equals("Bug") || label_cut.get(i).equals("bug") || label_cut.get(i).equals("BUG")){
                        btn_label4.setBackgroundResource(R.drawable.back_label_red);
                    }
                    break;
                case 4:
                    btn_label5.setVisibility(View.VISIBLE);
                    btn_label5.setText(label_cut.get(i));
                    btn_label5.setBackgroundResource(R.drawable.back_label);
                    if (label_cut.get(i).equals("Bug") || label_cut.get(i).equals("bug") || label_cut.get(i).equals("BUG")){
                        btn_label5.setBackgroundResource(R.drawable.back_label_red);
                    }
                    break;
                case 5:
                    btn_label6.setVisibility(View.VISIBLE);
                    btn_label6.setText(label_cut.get(i));
                    btn_label6.setBackgroundResource(R.drawable.back_label);
                    if (label_cut.get(i).equals("Bug") || label_cut.get(i).equals("bug") || label_cut.get(i).equals("BUG")){
                        btn_label6.setBackgroundResource(R.drawable.back_label_red);
                    }
                    break;
                case 6:
                    btn_label7.setVisibility(View.VISIBLE);
                    btn_label7.setText(label_cut.get(i));
                    btn_label7.setBackgroundResource(R.drawable.back_label);
                    if (label_cut.get(i).equals("Bug") || label_cut.get(i).equals("bug") || label_cut.get(i).equals("BUG")){
                        btn_label7.setBackgroundResource(R.drawable.back_label_red);
                    }
                    break;
                case 7:
                    btn_label8.setVisibility(View.VISIBLE);
                    btn_label8.setText(label_cut.get(i));
                    btn_label8.setBackgroundResource(R.drawable.back_label);
                    if (label_cut.get(i).equals("Bug") || label_cut.get(i).equals("bug") || label_cut.get(i).equals("BUG")){
                        btn_label8.setBackgroundResource(R.drawable.back_label_red);
                    }
                    break;
                case 8:
                    btn_label9.setVisibility(View.VISIBLE);
                    btn_label9.setText(label_cut.get(i));
                    btn_label9.setBackgroundResource(R.drawable.back_label);
                    if (label_cut.get(i).equals("Bug") || label_cut.get(i).equals("bug") || label_cut.get(i).equals("BUG")){
                        btn_label9.setBackgroundResource(R.drawable.back_label_red);
                    }
                    break;
                case 9:
                    btn_label10.setVisibility(View.VISIBLE);
                    btn_label10.setText(label_cut.get(i));
                    btn_label10.setBackgroundResource(R.drawable.back_label);
                    if (label_cut.get(i).equals("Bug") || label_cut.get(i).equals("bug") || label_cut.get(i).equals("BUG")){
                        btn_label10.setBackgroundResource(R.drawable.back_label_red);
                    }
                    break;

            }
            i++;
        }
    }


    void editTask(){
        Intent i = new Intent(TaskDetail.this, UpdateTask.class);
        startActivity(i);
    }

    void deleteTask(){
        new DeleteTask().execute();
    }

    void showToast(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detailtask, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_edittask) {
            editTask();
            return true;
        }
        if (id == R.id.action_deletetask) {
            showAlert();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class DeleteTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(TaskDetail.this);
            pDialog.setMessage("Deleting Data ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                URLConnection urlconnection = new URLConnection();
                responsemsg = urlconnection.deleteMethod(Data.uriTask, Task.task_id);
                JSONObject row = new JSONObject(responsemsg);
                parseResponse = row.getString("title") + " : " + row.getString("msg");

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showToast(parseResponse);
                    Intent i = new Intent(TaskDetail.this, MainActivity.class);
                    startActivity(i);
                    TaskDetail.this.finish();
                }
            });
        }
    }


}
