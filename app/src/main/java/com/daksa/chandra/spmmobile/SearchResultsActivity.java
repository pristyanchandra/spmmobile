package com.daksa.chandra.spmmobile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

public class SearchResultsActivity extends ActionBarActivity {

    private Toolbar topToolBar;
    private TextView txtQuery;
 
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);

        topToolBar = (Toolbar)findViewById(R.id.toolbar_search);
        setSupportActionBar(topToolBar);
//        topToolBar.setLogoDescription(getResources().getString(R.string.hasil_cari));
        getSupportActionBar().setTitle(R.string.hasil_cari);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        txtQuery = (TextView) findViewById(R.id.txtQuery);

        Intent in = getIntent();
        Bundle b = in.getExtras();

        if(b!=null) {
            String query =(String) b.get("query");
            txtQuery.setText(query);
        }
    }

 
    /**
     * Handling intent data
     */

}