package com.daksa.chandra.spmmobile;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.daksa.chandra.tab.DetailPagerAdapter;
import com.daksa.chandra.controller.Task;
import com.daksa.chandra.controller.URLConnection;
import com.daksa.chandra.data.Data;
import com.daksa.chandra.tab.SlidingTabLayout;

import org.json.JSONObject;

/**
 * Created by User on 9/14/2015.
 */
public class FragmentDetail extends ActionBarActivity{
    private Toolbar topToolBar;
    private ProgressDialog pDialog;
    String responsemsg = "";
    String parseResponse = "";
    ViewPager pager;
    DetailPagerAdapter adapter;
    SlidingTabLayout tabs;
    CharSequence Titles[]={ "Task Detail", "Attachment", "Comment", "Change Log" };
    int Numboftabs = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_taskdetail);
        topToolBar = (Toolbar)findViewById(R.id.toolbar_taskdetail);
        setSupportActionBar(topToolBar);
        getSupportActionBar().setTitle(R.string.task_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        adapter =  new DetailPagerAdapter(getSupportFragmentManager(),Titles,Numboftabs);
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);
        pager.setOffscreenPageLimit(3);
        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true);

        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.icons);
            }
        });

        tabs.setViewPager(pager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detailtask, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_edittask) {
            editTask();
            return true;
        }
        if (id == R.id.action_deletetask) {
            showAlert();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    void deleteTask(){
        new DeleteTask().execute();
    }

    void editTask(){
        Intent i = new Intent(FragmentDetail.this, UpdateTask.class);
        startActivity(i);
    }

    void showAlert(){
        AlertDialog.Builder alertdialog = new AlertDialog.Builder(FragmentDetail.this);
        alertdialog.setTitle("Confirmation");
        alertdialog.setMessage("Are you sure to delete this task?");
        alertdialog.setIcon(R.drawable.ic_warning_black_24dp);
        alertdialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteTask();
            }
        });
        alertdialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertdialog.show();
    }

    void showToast(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    private class DeleteTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(FragmentDetail.this);
            pDialog.setMessage("Deleting Data ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                URLConnection urlconnection = new URLConnection();
                responsemsg = urlconnection.deleteMethod(Data.uriTask, Task.task_id);
                JSONObject row = new JSONObject(responsemsg);
                parseResponse = row.getString("title") + " : " + row.getString("msg");

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showToast(parseResponse);
                    Intent i = new Intent(FragmentDetail.this, MainActivity.class);
                    startActivity(i);
                    FragmentDetail.this.finish();
                }
            });
        }
    }
}
