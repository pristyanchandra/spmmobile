package com.daksa.chandra.spmmobile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.daksa.chandra.controller.Task;
import com.daksa.chandra.controller.URLConnection;
import com.daksa.chandra.data.Data;
import com.daksa.chandra.tab.SlidingTabLayout;
import com.daksa.chandra.tab.AddTaskPagerAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by User on 9/21/2015.
 */
public class FragmentAdd extends ActionBarActivity {
    private Toolbar topToolBar;
    ProgressDialog pDialog;
    ViewPager pager;
    AddTaskPagerAdapter adapter;
    SlidingTabLayout tabs;
    Button btn_submit;
    CharSequence Titles[]={ "Task Info", "Labels", "Assignees", "Related Tasks" };
    int Numboftabs = 4;

    String responsemsg;
    String parseResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Get the view from activity_main.xml
        setContentView(R.layout.fragment_addtask);
        topToolBar = (Toolbar)findViewById(R.id.toolbar_addtask);
        setSupportActionBar(topToolBar);
        getSupportActionBar().setTitle(R.string.add_newtask);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        Data.fNumber = 1;
        Data.list_assignees = new ArrayList<>();
        Data.list_related = new ArrayList<>();

        btn_submit = (Button) findViewById(R.id.btn_addtask);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SendTask().execute();
            }
        });

        adapter =  new AddTaskPagerAdapter(getSupportFragmentManager(),Titles,Numboftabs);
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);
        pager.setOffscreenPageLimit(3);
        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true);

        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.icons);
            }
        });

        tabs.setViewPager(pager);
        Data.list_assignees = new ArrayList<>();
    }

    void showToast(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    private class SendTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(FragmentAdd.this);
            pDialog.setMessage("Sending Data ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                // Create parameters JSONObject
                JSONObject parameters = new JSONObject();
                parameters.put("title", Task.title);
                parameters.put("description", Task.description);
                parameters.put("owner", Task.owner);
                parameters.put("workspace", Task.workspace);
                parameters.put("labels", Data.getLabel());
                parameters.put("due_date", Task.date+" "+Task.time);
                parameters.put("assignees", Data.getAsg());
                parameters.put("estimation_time", Task.estimation);
                parameters.put("related_task", Data.getRelated());

                URLConnection urlconnection = new URLConnection();
                responsemsg = urlconnection.postMethod(Data.uriTask, parameters.toString());
                JSONObject row = new JSONObject(responsemsg);
                parseResponse = row.getString("msg");

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showToast(parseResponse);
                    Data.clearAdd();
                    Intent i = new Intent(FragmentAdd.this, MainActivity.class);
                    startActivity(i);
                }
            });
        }
    }
}
