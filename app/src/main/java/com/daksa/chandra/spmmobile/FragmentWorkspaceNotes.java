package com.daksa.chandra.spmmobile;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.daksa.chandra.controller.NotesAdapter;
import com.daksa.chandra.controller.URLConnection;
import com.daksa.chandra.controller.Workspace;
import com.daksa.chandra.data.Data;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 9/23/2015.
 */
public class FragmentWorkspaceNotes extends Fragment {

    ListView listview;
    ImageButton btn_tambah;

    String content;
    List<String> list_id;
    List<String> list_title;
    List<String> list_content;
    List<String> list_created;
    List<String> list_modified;

    Handler h = new Handler();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_workspace_notes, container, false);
        listview = (ListView) view.findViewById(R.id.list_notes);
        btn_tambah = (ImageButton) view.findViewById(R.id.btn_tambah_notes);
        return view;
    }

    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        h.removeCallbacksAndMessages(null);
        try{
            new JSONParse().execute();
        } catch (Exception e) {
            showToast(""+e);
        }

    }

    void showToast(String msg){
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    private class JSONParse extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... args) {
            URLConnection urlconn = new URLConnection();
            content = urlconn.getMethod(Data.uriWorkspace+""+ Workspace.id_static+"/notes");
            return null;

        }

        @Override
        protected void onPostExecute(String result) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject obj = new JSONObject(content);
                        JSONArray arr = (JSONArray) obj.getJSONArray("notes");
                        Data.task.clear();
                        list_id = new ArrayList<String>();
                        list_title = new ArrayList<>();
                        list_created = new ArrayList<>();
                        list_modified = new ArrayList<>();
                        list_content = new ArrayList<>();
                        for (int i = 0; i < arr.length(); i++){
                            JSONObject row = arr.getJSONObject(i);
                            list_id.add(row.getString("id_note"));
                            list_title.add(row.getString("title"));
                            list_content.add(row.getString("content"));
                            list_created.add(row.getString("createdtime"));
                            list_modified.add(row.getString("lastmodified"));

                            try {
                                listview.setAdapter(new NotesAdapter(getActivity(), list_id, list_title, list_content, list_created, list_modified));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    } catch (Exception e) {
//                        showToast(content);
                        showToast(""+e);
                        Log.e("ERROR", "run ", e);
                    }
                }
            });
        }
    }

}
