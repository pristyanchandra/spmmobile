package com.daksa.chandra.spmmobile;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.daksa.chandra.controller.Note;

/**
 * Created by User on 9/25/2015.
 */
public class NoteDetail extends AppCompatActivity {
    private Toolbar topToolBar;
    TextView txt_title;
    TextView txt_content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.note_detail);
        topToolBar = (Toolbar) findViewById(R.id.toolbar_notedetail);
        setSupportActionBar(topToolBar);
        getSupportActionBar().setTitle("Note");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        txt_content = (TextView) findViewById(R.id.txt_notedetail_content);
        txt_title = (TextView) findViewById(R.id.txt_notedetail_title);

        txt_title.setText(Note.title);
        txt_content.setText(Note.content);
    }
}
