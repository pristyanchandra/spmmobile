package com.daksa.chandra.spmmobile;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.daksa.chandra.controller.SimpleFileDialog;
import com.daksa.chandra.controller.Task;
import com.daksa.chandra.controller.URLConnection;
import com.daksa.chandra.data.Data;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by User on 9/14/2015.
 */
public class FragmentTaskAttachment extends Fragment {

    ListView listview;
    EditText txt_file;
    ImageButton btn_send_attach;
    ImageButton btn_file;
    ProgressDialog pDialog;

    String responsemsg = "";
    String file = "";
    String l_name = "";
    String l_uploader = "";
    String l_date = "";
    Handler h = new Handler();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_attach, container, false);
        listview = (ListView)view.findViewById(R.id.list_attach_task);
        txt_file = (EditText) view.findViewById(R.id.txt_file_task);
        btn_file = (ImageButton) view.findViewById(R.id.btn_openfile_task);
        Data.fNumber = 1;
        return view;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        h.removeCallbacksAndMessages(null);
        btn_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SimpleFileDialog FileOpenDialog = new SimpleFileDialog(getActivity(), "FileOpen", new SimpleFileDialog.SimpleFileDialogListener() {
                    @Override
                    public void onChosenDir(String chosenDir) {
                        String m_chosen = "";
                        m_chosen = chosenDir;
                        Toast.makeText(getActivity(), "Chosen FileOpenDialog File: " + m_chosen, Toast.LENGTH_LONG).show();
                        txt_file.setText(m_chosen);
                    }
                });
                FileOpenDialog.Default_File_Name = "";
                FileOpenDialog.chooseFile_or_Dir();
            }
        });
        try {
            new GetAttach().execute();
        } catch (Exception e){
            e.printStackTrace();
            showToast("Error : " + e);
        }

    }

    void showToast(String msg){
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    private class GetAttach extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... args) {
            URLConnection urlconn = new URLConnection();
            responsemsg = urlconn.getMethod(Data.uriAttach+""+ Task.task_id);
            return null;

        }

        @Override
        protected void onPostExecute(String result) {
//            pDialog.dismiss();
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject obj = new JSONObject(responsemsg);
                        JSONArray arr = (JSONArray) obj.getJSONArray("attachment");
                        ArrayList<HashMap<String, String>> attachlist = new ArrayList<>();
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject row = arr.getJSONObject(i);
                            l_name = row.getString("nama_file");
                            l_uploader = row.getString("uploader");
                            l_date = row.getString("date");

                            HashMap<String, String> map = new HashMap<>();
//                            attachlist.remove(map);
                            map.put("nama_file", l_name);
                            map.put("uploader", l_uploader);
                            map.put("date", l_date);
                            attachlist.add(map);

                            ListAdapter adapter = new SimpleAdapter(getActivity(), attachlist, R.layout.list_attachment,
                                    new String[]{"nama_file", "uploader", "date"},
                                    new int[]{R.id.txt_attach_name, R.id.txt_attach_uploader, R.id.txt_attach_date});
                            listview.setAdapter(adapter);

                            listview.startLayoutAnimation();
                        }

                    } catch (Exception e) {
                        showToast("" + e);
                        Log.e("ERROR", "run ", e);
                    }
                }
            });
        }
    }


}
