package com.daksa.chandra.spmmobile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.daksa.chandra.controller.SimpleFileDialog;
import com.daksa.chandra.controller.Task;
import com.daksa.chandra.controller.URLConnection;
import com.daksa.chandra.data.Data;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by User on 9/8/2015.
 */
public class TaskAttach extends ActionBarActivity {
    private Toolbar topToolBar;
    ListView listview;
    EditText txt_file;
    ImageButton btn_send_attach;
    ImageButton btn_file;
    ProgressDialog pDialog;

    String responsemsg = "";
    String file = "";
    String l_name = "";
    String l_uploader = "";
    String l_date = "";
    LayoutAnimationController lac;

    ArrayList<HashMap<String, String>> attachlist = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.task_attachment);
        topToolBar = (Toolbar) findViewById(R.id.toolbar_taskattach);
        setSupportActionBar(topToolBar);
        getSupportActionBar().setTitle(R.string.attach);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        listview = (ListView) findViewById(R.id.list_attach_task);
        txt_file = (EditText) findViewById(R.id.txt_file_task);
        btn_file = (ImageButton) findViewById(R.id.btn_openfile_task);
        Data.fNumber = 1;
        btn_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SimpleFileDialog FileOpenDialog = new SimpleFileDialog(TaskAttach.this, "FileOpen", new SimpleFileDialog.SimpleFileDialogListener() {
                    @Override
                    public void onChosenDir(String chosenDir) {
                        // The code in this function will be executed when the dialog OK button is pushed
                        String m_chosen = "";
                        m_chosen = chosenDir;
                        Toast.makeText(TaskAttach.this, "Chosen FileOpenDialog File: " + m_chosen, Toast.LENGTH_LONG).show();
                        txt_file.setText(m_chosen);
                    }
                });
                FileOpenDialog.Default_File_Name = "";
                FileOpenDialog.chooseFile_or_Dir();
            }
        });
        try{
            new GetAttach().execute();
        } catch (Exception e){
            e.printStackTrace();
            showToast("Error : "+e);
        }

    }

    void showToast(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }


    private class GetAttach extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(TaskAttach.this);
            pDialog.setMessage("Retrieving Data ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {
            URLConnection urlconn = new URLConnection();
            responsemsg = urlconn.getMethod(Data.uriAttach+""+Task.task_id);
            return null;

        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject obj = new JSONObject(responsemsg);
                        JSONArray arr = (JSONArray) obj.getJSONArray("attachment");

                        for (int i = 0; i < arr.length(); i++){
                            JSONObject row = arr.getJSONObject(i);
                            l_name = row.getString("nama_file");
                            l_uploader = row.getString("uploader");
                            l_date = row.getString("date");

                            HashMap<String, String> map = new HashMap<>();
                            map.put("nama_file", l_name);
                            map.put("uploader", l_uploader);
                            map.put("date", l_date);
                            attachlist.add(map);

                            ListAdapter adapter = new SimpleAdapter(TaskAttach.this, attachlist, R.layout.list_attachment,
                                    new String[]{"nama_file","uploader","date"},
                                    new int[]{R.id.txt_attach_name, R.id.txt_attach_uploader, R.id.txt_attach_date});
                            listview.setAdapter(adapter);
                            lac = new LayoutAnimationController(AnimationUtils.loadAnimation(TaskAttach.this, R.anim.slide_down), 0.5f); //0.5f == time between appearance of listview items.
                            listview.setLayoutAnimation(lac);
                            listview.startLayoutAnimation();
                        }

                    } catch (Exception e) {
                        showToast(""+e);
                        Log.e("ERROR", "run ", e);
                    }
//                    showToast(responsemsg);
                }
            });
        }
    }


}
