package com.daksa.chandra.spmmobile;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;

import com.daksa.chandra.controller.LabelsAdapter;
import com.daksa.chandra.controller.URLConnection;
import com.daksa.chandra.data.Data;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 9/21/2015.
 */
public class FragmentTaskLabels extends Fragment {

    Handler h = new Handler();
    String label_response = "";
    ImageButton btn_label;
    ListView listview;
    Spinner spn_label;
    List<String> list_text;
    List<String> list_color;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_labels, container, false);
        spn_label = (Spinner) view.findViewById(R.id.spn_addtask_labels);
        listview = (ListView) view.findViewById(R.id.lv_labels);
        btn_label = (ImageButton) view.findViewById(R.id.btn_addtask_labels);
        Data.fNumber = 1;
        return view;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        h.removeCallbacksAndMessages(null);
        new GetLabels().execute();

        btn_label.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Data.list_text.add(spn_label.getSelectedItem().toString());
                Data.list_color.add(searchColor(spn_label.getSelectedItem().toString()));
                setList();
            }
        });
    }

    String searchColor(String key){
        String color = "";
        int position = 0;
        int i = 0;
        while (i < list_color.size()){
            if (list_text.get(i).equals(key)){
                position = i;
                color = list_color.get(position);
            }
            i++;
        }


        return color;
    }

    void setList(){
        listview.setAdapter(new LabelsAdapter(getActivity(), Data.list_text, Data.list_color));
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                removeList(position);
            }
        });
    }

    void removeList(int position){
        Data.list_color.remove(position);
        Data.list_text.remove(position);
        setList();
    }

    private class GetLabels extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                URLConnection urlconnection = new URLConnection();
                label_response = urlconnection.getMethod(Data.uriLabelWorkspace+"1");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        list_text = new ArrayList<String>();
                        list_color = new ArrayList<String>();
                        JSONObject obj = new JSONObject(label_response);
                        JSONArray arr = (JSONArray) obj.getJSONArray("labels");
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject row = arr.getJSONObject(i);
                            list_text.add(row.getString("label"));
                            list_color.add(row.getString("color"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    ArrayAdapter<String> labelAdapter = new ArrayAdapter<String>(getActivity(),
                            android.R.layout.simple_spinner_item, list_text);
                    labelAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spn_label.setAdapter(labelAdapter);
                }
            });
        }
    }
}
