package com.daksa.chandra.spmmobile;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.daksa.chandra.tab.DetailPagerAdapter;
import com.daksa.chandra.tab.SlidingTabLayout;
import com.daksa.chandra.tab.WorkspacePagerAdapter;

/**
 * Created by User on 9/14/2015.
 */
public class FragmentWorkspaceDetail extends ActionBarActivity{
    private Toolbar topToolBar;
    ViewPager pager;
    WorkspacePagerAdapter adapter;
    SlidingTabLayout tabs;
    CharSequence Titles[]={ "Task", "Notes", "File", "Workspace Settings" };
    int Numboftabs = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_workspacedetail);
        topToolBar = (Toolbar)findViewById(R.id.toolbar_workspacedetail);
        setSupportActionBar(topToolBar);
        getSupportActionBar().setTitle(R.string.workspace_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        adapter =  new WorkspacePagerAdapter(getSupportFragmentManager(),Titles,Numboftabs);
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);
        pager.setOffscreenPageLimit(3);
        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true);

        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.icons);
            }
        });

        tabs.setViewPager(pager);
    }
}
