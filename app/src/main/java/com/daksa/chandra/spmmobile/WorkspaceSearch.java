package com.daksa.chandra.spmmobile;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;

/**
 * Created by User on 9/16/2015.
 */
public class WorkspaceSearch extends ActionBarActivity {
    private Toolbar topToolBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.workspace_search);
        topToolBar = (Toolbar) findViewById(R.id.toolbar_searchworkspace);
        setSupportActionBar(topToolBar);
        getSupportActionBar().setTitle(R.string.search_workspace);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

    }
}
