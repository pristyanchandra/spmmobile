package com.daksa.chandra.spmmobile;

import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ExpandableListAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.daksa.chandra.controller.LogData;
import com.daksa.chandra.controller.Task;
import com.daksa.chandra.controller.URLConnection;
import com.daksa.chandra.data.Data;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by User on 9/14/2015.
 */
public class FragmentTaskLog extends Fragment {

    String responsemsg = "";
    String change = "";
    String user = "";
    String time = "";
    String object = "";
    String from = "";
    String to = "";

    Handler h = new Handler();
    ListView listview;
    ExpandableListAdapter extlistadapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_tasklog, container, false);
        listview = (ListView) view.findViewById(R.id.lv_log);
        return view;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        h.removeCallbacksAndMessages(null);
        try{
            new GetLog().execute();
        } catch (Exception e){
            e.printStackTrace();
            showToast("Error : "+e);
        }
    }

    void showToast(String msg){
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    private class GetLog extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... args) {
            URLConnection urlconn = new URLConnection();
            responsemsg = urlconn.getMethod(Data.uriLog+""+ Task.task_id);
            return null;

        }

        @Override
        protected void onPostExecute(String result) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject obj = new JSONObject(responsemsg);
                        JSONArray arr = (JSONArray) obj.getJSONArray("log");
                        ArrayList<HashMap<String, String>> loglist = new ArrayList<>();
                        LogData.clear();
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject row = arr.getJSONObject(i);
                            change = row.getString("changes");
                            user = row.getString("user");
                            time = row.getString("time");
                            object = row.getString("object");
                            from = row.getString("from");
                            to = row.getString("to");

                            HashMap<String, String> map = new HashMap<>();
                            map.put("user", user);
                            map.put("changes", change);
                            map.put("time", time);
                            loglist.add(map);

                            LogData.object.add(object);
                            LogData.from.add(from);
                            LogData.to.add(to);
                            ListAdapter adapter = new SimpleAdapter(getActivity(), loglist, R.layout.list_log,
                                    new String[]{"user", "changes", "time"},
                                    new int[]{R.id.tv_log_user, R.id.tv_log_change, R.id.tv_log_time});
                            listview.setAdapter(adapter);

                            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    final Dialog dialog = new Dialog(getActivity());
                                    dialog.setContentView(R.layout.dialog_log);
                                    dialog.setTitle(LogData.object.get(position));
                                    dialog.closeOptionsMenu();
                                    dialog.setCancelable(true);
                                    TextView from = (TextView) dialog.findViewById(R.id.tv_dialog_from);
                                    from.setText(LogData.from.get(position));
                                    TextView to = (TextView) dialog.findViewById(R.id.tv_dialog_to);
                                    to.setText(LogData.to.get(position));
                                    dialog.show();
                                }
                            });
                        }
                    } catch (Exception e) {
                        showToast(responsemsg);
                        showToast("" + e);
                        Log.e("ERROR", "run ", e);
                    }
                }
            });
        }
    }
}
