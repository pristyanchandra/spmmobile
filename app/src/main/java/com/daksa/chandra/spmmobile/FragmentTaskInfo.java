package com.daksa.chandra.spmmobile;

import android.app.DialogFragment;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.daksa.chandra.controller.DateConversion;
import com.daksa.chandra.controller.Task;
import com.daksa.chandra.controller.URLConnection;
import com.daksa.chandra.data.Data;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by User on 9/21/2015.
 */
public class FragmentTaskInfo extends Fragment implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    Handler h = new Handler();
    EditText title;
    EditText description;
    AppCompatAutoCompleteTextView owner;
    TextView workspace;
    TextView date;
    TextView time;
    EditText estimation;
    ImageButton btn_date;
    ImageButton btn_time;

    String employee_response;
    List<String> employee_list = new ArrayList<>();
    ArrayAdapter<String> employee_adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_taskinfo, container, false);
        title = (EditText) view.findViewById(R.id.txt_addtask_title);
        description = (EditText) view.findViewById(R.id.txt_addtask_description);
        owner = (AppCompatAutoCompleteTextView) view.findViewById(R.id.auto_addtask_owner);
        workspace = (TextView) view.findViewById(R.id.tv_addtask_workspace);
        date = (TextView) view.findViewById(R.id.tv_addtask_date);
        time = (TextView) view.findViewById(R.id.tv_addtask_time);
        estimation = (EditText) view.findViewById(R.id.txt_addtask_estimation);
        btn_date = (ImageButton) view.findViewById(R.id.btn_addtask_date);
        btn_time = (ImageButton) view.findViewById(R.id.btn_addtask_time);
        Data.fNumber = 1;
        return view;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        h.removeCallbacksAndMessages(null);

        title.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Task.title = "" + s;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        description.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Task.description = ""+s;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        owner.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Task.owner = ""+s;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        estimation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Task.estimation = ""+s;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        time.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Task.time = "" + s;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        date.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Task.date = "" + s;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btn_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        FragmentTaskInfo.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.setAccentColor(Color.parseColor("#00BCD4"));
                dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
            }
        });

        btn_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                TimePickerDialog tpd = TimePickerDialog.newInstance(FragmentTaskInfo.this,now.get(Calendar.HOUR_OF_DAY),now.get(Calendar.MINUTE),true);
                tpd.setAccentColor(Color.parseColor("#00BCD4"));
                tpd.show(getActivity().getFragmentManager(), "Timepickerdialog");
            }
        });
        new GetEmployee().execute();
    }

    void showToast(String msg){
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String datepicked = dayOfMonth+"-"+(++monthOfYear)+"-"+year;
        DateConversion dc = new DateConversion();
        date.setText(dc.conv(datepicked));
    }
    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
        String hourString = hourOfDay < 10 ? "0"+hourOfDay : ""+hourOfDay;
        String minuteString = minute < 10 ? "0"+minute : ""+minute;
        String timepicked = hourString+":"+minuteString;
        time.setText(timepicked);
    }

    private class GetEmployee extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                URLConnection urlconnection = new URLConnection();
                employee_response = urlconnection.getMethod(Data.uriGetAssignees+"1");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject obj = new JSONObject(employee_response);
                        JSONArray arr = (JSONArray) obj.getJSONArray("employee");
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject row = arr.getJSONObject(i);
                            employee_list.add(row.getString("employee_name"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    employee_adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, employee_list);
                    owner.setAdapter(employee_adapter);
                    owner.setThreshold(1);
                }
            });
        }
    }

}
