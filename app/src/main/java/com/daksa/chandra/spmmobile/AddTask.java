package com.daksa.chandra.spmmobile;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.daksa.chandra.controller.DateConversion;
import com.daksa.chandra.controller.SpinnerAdapter;
import com.daksa.chandra.controller.URLConnection;
import com.daksa.chandra.data.Data;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by User on 9/2/2015.
 */
public class AddTask extends ActionBarActivity implements
        TimePickerDialog.OnTimeSetListener,
        DatePickerDialog.OnDateSetListener {

    private Toolbar topToolBar;
    Button btn_addtask;
    ImageButton btn_labels;
    ImageButton btn_date;
    ImageButton btn_time;
    ImageButton btn_assignees;
    EditText txt_title;
    EditText txt_description;
    EditText txt_estimation;
    TextView txt_workspace;
    TextView txt_date;
    TextView txt_time;
    TextView txt_assigness;
    Spinner spn_labels;

    Button btn_label1;
    Button btn_label2;
    Button btn_label3;
    Button btn_label4;
    Button btn_label5;
    Button btn_label6;
    Button btn_label7;
    Button btn_label8;
    Button btn_label9;
    Button btn_label10;

    Button btn_asg1;
    Button btn_asg2;
    Button btn_asg3;
    Button btn_asg4;
    Button btn_asg5;
    Button btn_asg6;
    Button btn_asg7;
    Button btn_asg8;
    Button btn_asg9;
    Button btn_asg10;

    AppCompatAutoCompleteTextView auto_owner;
    AppCompatAutoCompleteTextView auto_assignees;
    ProgressDialog pDialog;

    String responsemsg;
    String parseResponse;

    String title = "";
    String wks = "";
    String due = "";
    String owner = "";
    String description = "";
    String estimation = "";
    String label = "";
    String date = "";
    String time = "";
    String assignees = "";

    String assignee_all = "";
    String label_all = "";
    int jumlah_label = 0;
    int jumlah_asg = 0;

    String employee_response = "";
    ArrayAdapter<String> employee_adapter;
    List<String> employee_list = new ArrayList<String>();

    ArrayList<String> lbl = new ArrayList<>();
    ArrayList<String> asg = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_task);
        topToolBar = (Toolbar)findViewById(R.id.toolbar_addtask);
        setSupportActionBar(topToolBar);
        getSupportActionBar().setTitle(R.string.newtask);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        new GetEmployee().execute();

        auto_owner = (AppCompatAutoCompleteTextView) findViewById(R.id.auto_addtask_owner);
        auto_assignees = (AppCompatAutoCompleteTextView) findViewById(R.id.auto_addtask_assignee);
        txt_title = (EditText) findViewById(R.id.txt_addtask_title);
        txt_description = (EditText) findViewById(R.id.txt_addtask_description);
        txt_estimation = (EditText) findViewById(R.id.txt_addtask_estimation);
        txt_workspace = (TextView) findViewById(R.id.tv_addtask_workspace);
        txt_date = (TextView) findViewById(R.id.tv_addtask_date);
        txt_time = (TextView) findViewById(R.id.tv_addtask_time);
        txt_assigness = (TextView) findViewById(R.id.txt_addtask_assignees);

        spn_labels = (Spinner) findViewById(R.id.spn_addtask_labels);

        btn_labels = (ImageButton) findViewById(R.id.btn_addtask_labels);
        btn_date = (ImageButton) findViewById(R.id.btn_addtask_date);
        btn_time = (ImageButton) findViewById(R.id.btn_addtask_time);
        btn_assignees = (ImageButton) findViewById(R.id.btn_addtask_assignees);
        btn_addtask = (Button) findViewById(R.id.btn_addtask_send);

        // For labels & Assignees -----------------------------------------
        btn_label1 = (Button) findViewById(R.id.btn_addtask_label_1);
        btn_label2 = (Button) findViewById(R.id.btn_addtask_label_2);
        btn_label3 = (Button) findViewById(R.id.btn_addtask_label_3);
        btn_label4 = (Button) findViewById(R.id.btn_addtask_label_4);
        btn_label5 = (Button) findViewById(R.id.btn_addtask_label_5);
        btn_label6 = (Button) findViewById(R.id.btn_addtask_label_6);
        btn_label7 = (Button) findViewById(R.id.btn_addtask_label_7);
        btn_label8 = (Button) findViewById(R.id.btn_addtask_label_8);
        btn_label9 = (Button) findViewById(R.id.btn_addtask_label_9);
        btn_label10 = (Button) findViewById(R.id.btn_addtask_label_10);

        btn_asg1 = (Button) findViewById(R.id.btn_addtask_asg_1);
        btn_asg2 = (Button) findViewById(R.id.btn_addtask_asg_2);
        btn_asg3 = (Button) findViewById(R.id.btn_addtask_asg_3);
        btn_asg4 = (Button) findViewById(R.id.btn_addtask_asg_4);
        btn_asg5 = (Button) findViewById(R.id.btn_addtask_asg_5);
        btn_asg6 = (Button) findViewById(R.id.btn_addtask_asg_6);
        btn_asg7 = (Button) findViewById(R.id.btn_addtask_asg_7);
        btn_asg8 = (Button) findViewById(R.id.btn_addtask_asg_8);
        btn_asg9 = (Button) findViewById(R.id.btn_addtask_asg_9);
        btn_asg10 = (Button) findViewById(R.id.btn_addtask_asg_10);

        setAssigneeGone();
        setLabelGone();
        // ---------------------------------------------------------------

        // For Label Spinner ------------------------------
        SpinnerAdapter su = new SpinnerAdapter();
        ArrayAdapter<String> labelAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, su.getListLabel());
        labelAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_labels.setAdapter(labelAdapter);
        // ------------------------------------------

        btn_addtask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                title = txt_title.getText().toString();
                description = txt_description.getText().toString();
                owner = auto_owner.getText().toString();
                wks = txt_workspace.getText().toString();
                DateConversion dc = new DateConversion();
                try {
                    time = txt_time.getText().toString();
                    date = txt_date.getText().toString();
                    due = dc.conv(date)+" "+time;
                } catch (Exception e) {
                    showToast("Date / Time not set");
                }

                estimation = txt_estimation.getText().toString();
                label = label_all;
                assignees = assignee_all;
//                showToast(title + " | " + wks + " | " + description + " | " + owner + " | " + due + " | " + label + " | " + estimation + " | " + assignees);
                new SendTask().execute();
            }
        });

        btn_labels.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String label = spn_labels.getSelectedItem().toString();
                if (jumlah_label < 10){
                    if (label_all.equals("")){
                        jumlah_label++;
                        lbl.add(label);
                    } else {
                        jumlah_label++;
                        lbl.add(label);
                    }
                    setLabelListener();
                } else {
                    showToast("Maksimal 10 label");
                }

            }
        });

        btn_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        AddTask.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.setAccentColor(Color.parseColor("#00BCD4"));
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });

        btn_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                TimePickerDialog tpd = TimePickerDialog.newInstance(AddTask.this,now.get(Calendar.HOUR_OF_DAY),now.get(Calendar.MINUTE),true);
                tpd.setAccentColor(Color.parseColor("#00BCD4"));
                tpd.show(getFragmentManager(), "Timepickerdialog");
            }
        });

        btn_assignees.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String assignee = auto_assignees.getText().toString();
                if (jumlah_asg < 10){
                    if (assignee_all.equals("")){
                        jumlah_asg++;
                        asg.add(assignee);
                    } else {
                        jumlah_asg++;
                        asg.add(assignee);
                    }
                    setAssigneeListener();
                    auto_assignees.setText("");
                } else {
                    showToast("Maksimal 10 Assignees");
                }
            }
        });
    }

    void deleteLabel(int index){
        lbl.remove(index);
        setLabelGone();
        setLabelListener();
    }

    void deleteAssignee(int index){
        asg.remove(index);
        setAssigneeGone();
        setAssigneeListener();
    }

    void setLabelListener(){
        int x = 0;
        label_all = "";
        jumlah_label = lbl.size();
        while (x < lbl.size()){
            if (label_all.equals("")){
                label_all = lbl.get(x);
            } else {
                label_all = label_all + "," + lbl.get(x);
            }
            switch (x){
                case 0:
                    btn_label1.setVisibility(View.VISIBLE);
                    btn_label1.setText(lbl.get(x));
                    btn_label1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteLabel(0);
                        }
                    });
                    break;
                case 1:
                    btn_label2.setVisibility(View.VISIBLE);
                    btn_label2.setText(lbl.get(x));
                    btn_label2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            deleteLabel(1);
                        }
                    });
                    break;
                case 2:
                    btn_label3.setVisibility(View.VISIBLE);
                    btn_label3.setText(lbl.get(x));
                    btn_label3.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteLabel(2);
                        }
                    });
                    break;
                case 3:
                    btn_label4.setVisibility(View.VISIBLE);
                    btn_label4.setText(lbl.get(x));
                    btn_label4.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteLabel(3);
                        }
                    });
                    break;
                case 4:
                    btn_label5.setVisibility(View.VISIBLE);
                    btn_label5.setText(lbl.get(x));
                    btn_label5.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteLabel(4);
                        }
                    });
                    break;
                case 5:
                    btn_label6.setVisibility(View.VISIBLE);
                    btn_label6.setText(lbl.get(x));
                    btn_label6.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteLabel(5);
                        }
                    });
                    break;
                case 6:
                    btn_label7.setVisibility(View.VISIBLE);
                    btn_label7.setText(lbl.get(x));
                    btn_label7.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteLabel(6);
                        }
                    });
                    break;
                case 7:
                    btn_label8.setVisibility(View.VISIBLE);
                    btn_label8.setText(lbl.get(x));
                    btn_label8.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteLabel(7);
                        }
                    });
                    break;
                case 8:
                    btn_label9.setVisibility(View.VISIBLE);
                    btn_label9.setText(lbl.get(x));
                    btn_label9.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteLabel(8);
                        }
                    });
                    break;
                case 9:
                    btn_label10.setVisibility(View.VISIBLE);
                    btn_label10.setText(lbl.get(x));
                    btn_label10.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteLabel(9);
                        }
                    });
                    break;
            }
            x++;
        }
    }

    void setAssigneeListener(){
        int x = 0;
        assignee_all = "";
        jumlah_asg = asg.size();
        while (x < asg.size()){
            if (assignee_all.equals("")){
                assignee_all = asg.get(x);
            } else {
                assignee_all = assignee_all + "," + asg.get(x);
            }
            switch (x){
                case 0:
                    btn_asg1.setVisibility(View.VISIBLE);
                    btn_asg1.setText(asg.get(x));
                    btn_asg1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteAssignee(0);
                        }
                    });
                    break;
                case 1:
                    btn_asg2.setVisibility(View.VISIBLE);
                    btn_asg2.setText(asg.get(x));
                    btn_asg2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteAssignee(1);
                        }
                    });
                    break;
                case 2:
                    btn_asg3.setVisibility(View.VISIBLE);
                    btn_asg3.setText(asg.get(x));
                    btn_asg3.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteAssignee(2);
                        }
                    });
                    break;
                case 3:
                    btn_asg4.setVisibility(View.VISIBLE);
                    btn_asg4.setText(asg.get(x));
                    btn_asg4.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteAssignee(3);
                        }
                    });
                    break;
                case 4:
                    btn_asg5.setVisibility(View.VISIBLE);
                    btn_asg5.setText(asg.get(x));
                    btn_asg5.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteAssignee(4);
                        }
                    });
                    break;
                case 5:
                    btn_asg6.setVisibility(View.VISIBLE);
                    btn_asg6.setText(asg.get(x));
                    btn_asg6.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteAssignee(5);
                        }
                    });
                    break;
                case 6:
                    btn_asg7.setVisibility(View.VISIBLE);
                    btn_asg7.setText(asg.get(x));
                    btn_asg7.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteAssignee(6);
                        }
                    });
                    break;
                case 7:
                    btn_asg8.setVisibility(View.VISIBLE);
                    btn_asg8.setText(asg.get(x));
                    btn_asg8.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteAssignee(7);
                        }
                    });
                    break;
                case 8:
                    btn_asg9.setVisibility(View.VISIBLE);
                    btn_asg9.setText(asg.get(x));
                    btn_asg9.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteAssignee(8);
                        }
                    });
                    break;
                case 9:
                    btn_asg10.setVisibility(View.VISIBLE);
                    btn_asg10.setText(asg.get(x));
                    btn_asg10.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteAssignee(9);
                        }
                    });
                    break;
            }
            x++;
        }
    }

    void setLabelGone(){
        btn_label1.setVisibility(View.GONE);
        btn_label2.setVisibility(View.GONE);
        btn_label3.setVisibility(View.GONE);
        btn_label4.setVisibility(View.GONE);
        btn_label5.setVisibility(View.GONE);
        btn_label6.setVisibility(View.GONE);
        btn_label7.setVisibility(View.GONE);
        btn_label8.setVisibility(View.GONE);
        btn_label9.setVisibility(View.GONE);
        btn_label10.setVisibility(View.GONE);
    }

    void setAssigneeGone(){
        btn_asg1.setVisibility(View.GONE);
        btn_asg2.setVisibility(View.GONE);
        btn_asg3.setVisibility(View.GONE);
        btn_asg4.setVisibility(View.GONE);
        btn_asg5.setVisibility(View.GONE);
        btn_asg6.setVisibility(View.GONE);
        btn_asg7.setVisibility(View.GONE);
        btn_asg8.setVisibility(View.GONE);
        btn_asg9.setVisibility(View.GONE);
        btn_asg10.setVisibility(View.GONE);
    }

    void showToast(String msg){
        Toast.makeText(AddTask.this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = dayOfMonth+"-"+(++monthOfYear)+"-"+year;
        txt_date.setText(date);
    }
    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
        String hourString = hourOfDay < 10 ? "0"+hourOfDay : ""+hourOfDay;
        String minuteString = minute < 10 ? "0"+minute : ""+minute;
        String time = hourString+":"+minuteString;
        txt_time.setText(time);
    }

    @Override
    public void onResume() {
        super.onResume();
        TimePickerDialog tpd = (TimePickerDialog) getFragmentManager().findFragmentByTag("Timepickerdialog");
        DatePickerDialog dpd = (DatePickerDialog) getFragmentManager().findFragmentByTag("Datepickerdialog");

        if(tpd != null) tpd.setOnTimeSetListener(this);
        if(dpd != null) dpd.setOnDateSetListener(this);
    }

    private class SendTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(AddTask.this);
            pDialog.setMessage("Sending Data ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                // Create parameters JSONObject
                JSONObject parameters = new JSONObject();
                parameters.put("title", title);
                parameters.put("description", description);
                parameters.put("owner", owner);
                parameters.put("workspace", wks);
                parameters.put("labels", label);
                parameters.put("due_date", due);
                parameters.put("assignees", assignees);
                parameters.put("estimation_time", estimation);

                URLConnection urlconnection = new URLConnection();
                responsemsg = urlconnection.postMethod(Data.uriTask, parameters.toString());
                JSONObject row = new JSONObject(responsemsg);
                parseResponse = row.getString("msg");

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showToast(parseResponse);
                }
            });
        }
    }

    private class GetEmployee extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(AddTask.this);
            pDialog.setMessage("Retrieve Data ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                URLConnection urlconnection = new URLConnection();
                employee_response = urlconnection.getMethod(Data.uriGetAllEmployee);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject obj = new JSONObject(employee_response);
                        JSONArray arr = (JSONArray) obj.getJSONArray("employee");
                        for (int i = 0; i < arr.length(); i++){
                            JSONObject row = arr.getJSONObject(i);
                            employee_list.add(row.getString("employee_name"));
                        }
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                    employee_adapter = new ArrayAdapter<String>(AddTask.this, android.R.layout.simple_list_item_1, employee_list);
                    auto_owner.setAdapter(employee_adapter);
                    auto_owner.setThreshold(1);
                    auto_assignees.setAdapter(employee_adapter);
                    auto_assignees.setThreshold(1);
                }
            });
        }
    }

}
