package com.daksa.chandra.spmmobile;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ListView;

import com.daksa.chandra.controller.TaskAdapter;
import com.daksa.chandra.data.Data;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by User on 9/18/2015.
 */
public class TaskSearchResult extends ActionBarActivity {

    Toolbar topToolBar;
    ListView listview;
    String content;

    String task_id = "";
    String task_title = "";
    String task_workspace = "";
    String task_status = "";
    String task_due = "";
    String task_owner = "";
    String task_description = "";
    String task_created_time = "";
    String task_last_modified = "";
    String task_labels = "";
    String task_spent_time = "";
    String task_estimation_time = "";
    String task_assignees = "";

    ArrayList<String> t_id = new ArrayList<>();
    ArrayList<String> t_title = new ArrayList<>();
    ArrayList<String> t_workspace = new ArrayList<>();
    ArrayList<String> t_due = new ArrayList<>();
    ArrayList<String> t_spent = new ArrayList<>();
    ArrayList<String> t_label = new ArrayList<>();
    ArrayList<String> t_status = new ArrayList<>();
    ArrayList<String> t_assignees = new ArrayList<>();
    ArrayList<String> t_description= new ArrayList<>();
    ArrayList<String> t_owner = new ArrayList<>();
    ArrayList<String> t_createdtime = new ArrayList<>();
    ArrayList<String> t_lastmodified = new ArrayList<>();
    ArrayList<String> t_estimation = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.task_search_result);
        topToolBar = (Toolbar) findViewById(R.id.toolbar_tasksearchresult);
        setSupportActionBar(topToolBar);
        getSupportActionBar().setTitle(R.string.search_result);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        listview = (ListView) findViewById(R.id.list_task);
        setData();

    }

    void setData(){
        try {
            content = Data.searchResult;
            JSONObject obj = new JSONObject(content);
            JSONArray arr = (JSONArray) obj.getJSONArray(Data.TAG_TASK);
            Data.task.clear();
            t_id = new ArrayList<>();
            t_title = new ArrayList<>();
            t_workspace = new ArrayList<>();
            t_due = new ArrayList<>();
            t_spent = new ArrayList<>();
            t_label = new ArrayList<>();
            t_status = new ArrayList<>();
            t_assignees = new ArrayList<>();
            t_description= new ArrayList<>();
            t_owner = new ArrayList<>();
            t_createdtime = new ArrayList<>();
            t_lastmodified = new ArrayList<>();
            t_estimation = new ArrayList<>();
            for (int i = 0; i < arr.length(); i++){
                JSONObject row = arr.getJSONObject(i);

                task_id = row.getString(Data.TAG_TASK_ID);
                task_title = row.getString(Data.TAG_TASK_TITLE);
                task_workspace = row.getString(Data.TAG_TASK_WORKSPACE);
                task_due = row.getString(Data.TAG_TASK_DUE);
                task_status = row.getString(Data.TAG_TASK_STATUS);
                task_owner = row.getString(Data.TAG_TASK_OWNER);
                task_last_modified = row.getString(Data.TAG_TASK_MODIFIED);
                task_created_time = row.getString(Data.TAG_TASK_CREATED);
                task_description = row.getString(Data.TAG_TASK_DESCRIPTION);
                task_spent_time = row.getString(Data.TAG_TASK_SPENT);
                task_labels = row.getString(Data.TAG_TASK_LABELS);
                task_estimation_time = row.getString(Data.TAG_TASK_ESTIMATION);
                task_assignees = row.getString(Data.TAG_TASK_ASSIGNEES);

                t_id.add(task_id);
                t_title.add(task_title);
                t_workspace.add(task_workspace);
                t_due.add(task_due);
                t_status.add(task_status);
                t_spent.add(task_spent_time);
                t_label.add(task_labels);
                t_description.add(task_description);
                t_owner.add(task_owner);
                t_estimation.add(task_estimation_time);
                t_createdtime.add(task_created_time);
                t_lastmodified.add(task_last_modified);
                t_assignees.add(task_assignees);

                try {
                    listview.setAdapter(new TaskAdapter(TaskSearchResult.this, t_id, t_title, t_workspace, t_due, t_spent, t_label, t_status,
                            t_assignees, t_description, t_createdtime, t_lastmodified, t_estimation, t_owner));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
//            showToast(""+e);
            e.printStackTrace();
            Log.e("ERROR", "run ", e);
        }
    }
}
