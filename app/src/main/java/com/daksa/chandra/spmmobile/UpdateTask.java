package com.daksa.chandra.spmmobile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.daksa.chandra.controller.DateConversion;
import com.daksa.chandra.controller.SpinnerAdapter;
import com.daksa.chandra.controller.Task;
import com.daksa.chandra.controller.URLConnection;
import com.daksa.chandra.data.Data;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by User on 9/2/2015.
 */
public class UpdateTask extends ActionBarActivity implements
        TimePickerDialog.OnTimeSetListener,
        DatePickerDialog.OnDateSetListener {

    private Toolbar topToolBar;
    Button btn_update;
    ImageButton btn_labels;
    ImageButton btn_assignees;
    ImageButton btn_date;
    ImageButton btn_time;
    EditText txt_title;
    EditText txt_description;
    EditText txt_estimation;
    EditText txt_spent;
    TextView txt_workspace;
    TextView txt_date;
    TextView txt_time;
    Spinner spn_labels;
    AppCompatAutoCompleteTextView txt_owner;
    AppCompatAutoCompleteTextView txt_assignee;

    Button label_1;
    Button label_2;
    Button label_3;
    Button label_4;
    Button label_5;
    Button label_6;
    Button label_7;
    Button label_8;
    Button label_9;
    Button label_10;

    Button btn_asg1;
    Button btn_asg2;
    Button btn_asg3;
    Button btn_asg4;
    Button btn_asg5;
    Button btn_asg6;
    Button btn_asg7;
    Button btn_asg8;
    Button btn_asg9;
    Button btn_asg10;

    String cek_cut = "";
    int jumlah_label = 0;
    int jumlah_asg = 0;
    String label_all = "";
    String assignee_all = "";

    ProgressDialog pDialog;
    String employee_response;
    List<String> employee_list = new ArrayList<String>();
    ArrayAdapter<String> employee_adapter;
    ArrayList<String> asg = new ArrayList<>();
    ArrayList<String> lbl = new ArrayList<>();

    String id_task = "";
    String title_task = "";
    String description_task = "";
    String owner_task = "";
    String label_task = "";
    String workspace_task = "";
    String due_task = "";
    String assignee_task = "";
    String spent_task = "";
    String estimation_task = "";
    String date = "";
    String time = "";
    String responsemsg = "";
    String parseResponse = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_task);
        topToolBar = (Toolbar)findViewById(R.id.toolbar_updatetask);
        setSupportActionBar(topToolBar);
        getSupportActionBar().setTitle(R.string.updatetask);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        defComponent();
        setLabelGone();
        setAssigneeGone();
        setLabel();
        setLabelListener();
        setAssginees();
        setAssigneeListener();

        new GetEmployee().execute();

        txt_title.setText(Task.title);
        txt_description.setText(Task.description);
        txt_owner.setText(Task.owner);
        txt_estimation.setText(Task.estimation);
        txt_workspace.setText(Task.workspace);
        txt_date.setText(Task.date);
        txt_time.setText(Task.time);
        txt_spent.setText(Task.spent_time);
        SpinnerAdapter su = new SpinnerAdapter();
        ArrayAdapter<String> labelAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, su.getListLabel());
        labelAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_labels.setAdapter(labelAdapter);

        btn_labels.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String label = spn_labels.getSelectedItem().toString();
                if (jumlah_label < 10) {
                    if (label_all.equals("")) {
                        jumlah_label++;
                        lbl.add(label);
                    } else {
                        jumlah_label++;
                        lbl.add(label);
                    }
                    setLabelListener();
                } else {
                    showToast("Maksimal 10 label");
                }

            }
        });

        btn_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        UpdateTask.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.setAccentColor(Color.parseColor("#00BCD4"));
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });

        btn_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                TimePickerDialog tpd = TimePickerDialog.newInstance(UpdateTask.this,now.get(Calendar.HOUR_OF_DAY),now.get(Calendar.MINUTE),true);
                tpd.setAccentColor(Color.parseColor("#00BCD4"));
                tpd.show(getFragmentManager(), "Timepickerdialog");
            }
        });

        btn_assignees.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String assignee = txt_assignee.getText().toString();

                if (jumlah_asg < 10){
                    jumlah_asg++;
                    asg.add(assignee);
                    setAssigneeListener();
                    txt_assignee.setText("");
                } else {
                    showToast("Maksimal 10 Assignees");
                }
            }
        });

        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                id_task = Task.task_id;
                title_task = txt_title.getText().toString();
                description_task = txt_description.getText().toString();
                owner_task = txt_owner.getText().toString();
                workspace_task = txt_workspace.getText().toString();
                label_task = label_all;
                assignee_task = assignee_all;
                estimation_task = txt_estimation.getText().toString();
                spent_task = txt_spent.getText().toString();
                DateConversion dc = new DateConversion();
                try {
                    time = txt_time.getText().toString();
                    date = txt_date.getText().toString();
                    due_task = dc.conv(date)+" "+time;
                } catch (Exception e) {
                    showToast("Date / Time not set");
                }

                new sendUpdate().execute();
            }
        });
    }

    void defComponent(){
        txt_title = (EditText) findViewById(R.id.txt_updatetask_title);
        txt_description = (EditText) findViewById(R.id.txt_updatetask_description);
        txt_owner = (AppCompatAutoCompleteTextView) findViewById(R.id.txt_updatetask_owner);
        txt_assignee = (AppCompatAutoCompleteTextView) findViewById(R.id.auto_updatetask_assignee);
        txt_estimation = (EditText) findViewById(R.id.txt_updatetask_estimation);
        txt_estimation = (EditText) findViewById(R.id.txt_updatetask_estimation);
        txt_workspace = (TextView) findViewById(R.id.tv_updatetask_workspace);
        txt_date = (TextView) findViewById(R.id.tv_updatetask_date);
        txt_time = (TextView) findViewById(R.id.tv_updatetask_time);
        btn_labels = (ImageButton) findViewById(R.id.btn_updatetask_labels);
        btn_date = (ImageButton) findViewById(R.id.btn_updatetask_date);
        btn_time = (ImageButton) findViewById(R.id.btn_updatetask_time);
        btn_update = (Button) findViewById(R.id.btn_updatetask_send);
        spn_labels = (Spinner) findViewById(R.id.spn_updatetask_labels);
        btn_assignees = (ImageButton) findViewById(R.id.btn_updatetask_assignees);
        txt_spent = (EditText) findViewById(R.id.txt_updatetask_spent);

        label_1 = (Button) findViewById(R.id.btn_updatetask_label1);
        label_2 = (Button) findViewById(R.id.btn_updatetask_label2);
        label_3 = (Button) findViewById(R.id.btn_updatetask_label3);
        label_4 = (Button) findViewById(R.id.btn_updatetask_label4);
        label_5 = (Button) findViewById(R.id.btn_updatetask_label5);
        label_6 = (Button) findViewById(R.id.btn_updatetask_label6);
        label_7 = (Button) findViewById(R.id.btn_updatetask_label7);
        label_8 = (Button) findViewById(R.id.btn_updatetask_label8);
        label_9 = (Button) findViewById(R.id.btn_updatetask_label9);
        label_10 = (Button) findViewById(R.id.btn_updatetask_label10);

        btn_asg1 = (Button) findViewById(R.id.btn_updatetask_asg_1);
        btn_asg2 = (Button) findViewById(R.id.btn_updatetask_asg_2);
        btn_asg3 = (Button) findViewById(R.id.btn_updatetask_asg_3);
        btn_asg4 = (Button) findViewById(R.id.btn_updatetask_asg_4);
        btn_asg5 = (Button) findViewById(R.id.btn_updatetask_asg_5);
        btn_asg6 = (Button) findViewById(R.id.btn_updatetask_asg_6);
        btn_asg7 = (Button) findViewById(R.id.btn_updatetask_asg_7);
        btn_asg8 = (Button) findViewById(R.id.btn_updatetask_asg_8);
        btn_asg9 = (Button) findViewById(R.id.btn_updatetask_asg_9);
        btn_asg10 = (Button) findViewById(R.id.btn_updatetask_asg_10);
    }

    void setLabelGone(){
        label_1.setVisibility(View.GONE);
        label_2.setVisibility(View.GONE);
        label_3.setVisibility(View.GONE);
        label_4.setVisibility(View.GONE);
        label_5.setVisibility(View.GONE);
        label_6.setVisibility(View.GONE);
        label_7.setVisibility(View.GONE);
        label_8.setVisibility(View.GONE);
        label_9.setVisibility(View.GONE);
        label_10.setVisibility(View.GONE);
    }

    void setLabel(){
        lbl.clear();
        splitString(cutString(Task.labels), "label");
        clearString("label");
        int i = 0;
        while (i < lbl.size()){
            switch (i){
                case 0:
                    label_1.setVisibility(View.VISIBLE);
                    label_1.setText(lbl.get(i));
                    label_1.setBackgroundResource(R.drawable.back_label);
                    break;
                case 1:
                    label_2.setVisibility(View.VISIBLE);
                    label_2.setText(lbl.get(i));
                    label_2.setBackgroundResource(R.drawable.back_label);
                    break;
                case 2:
                    label_3.setVisibility(View.VISIBLE);
                    label_3.setText(lbl.get(i));
                    label_3.setBackgroundResource(R.drawable.back_label);
                    break;
                case 3:
                    label_4.setVisibility(View.VISIBLE);
                    label_4.setText(lbl.get(i));
                    label_4.setBackgroundResource(R.drawable.back_label);
                    break;
                case 4:
                    label_5.setVisibility(View.VISIBLE);
                    label_5.setText(lbl.get(i));
                    label_5.setBackgroundResource(R.drawable.back_label);
                    break;
                case 5:
                    label_6.setVisibility(View.VISIBLE);
                    label_6.setText(lbl.get(i));
                    label_6.setBackgroundResource(R.drawable.back_label);
                    break;
                case 6:
                    label_7.setVisibility(View.VISIBLE);
                    label_7.setText(lbl.get(i));
                    label_7.setBackgroundResource(R.drawable.back_label);
                    break;
                case 7:
                    label_8.setVisibility(View.VISIBLE);
                    label_8.setText(lbl.get(i));
                    label_8.setBackgroundResource(R.drawable.back_label);
                    break;
                case 8:
                    label_9.setVisibility(View.VISIBLE);
                    label_9.setText(lbl.get(i));
                    label_9.setBackgroundResource(R.drawable.back_label);
                    break;
                case 9:
                    label_10.setVisibility(View.VISIBLE);
                    label_10.setText(lbl.get(i));
                    label_10.setBackgroundResource(R.drawable.back_label);
                    break;

            }
            i++;
        }
//        lbl = data_cut;
    }

    void setLabelListener(){
        int x = 0;
        label_all = "";
        jumlah_label = lbl.size();
        while (x < lbl.size()){
            if (label_all.equals("")){
                label_all = lbl.get(x);
            } else {
                label_all = label_all + "," + lbl.get(x);
            }
            switch (x){
                case 0:
                    label_1.setVisibility(View.VISIBLE);
                    label_1.setText(lbl.get(x));
                    label_1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteLabel(0);
                        }
                    });
                    break;
                case 1:
                    label_2.setVisibility(View.VISIBLE);
                    label_2.setText(lbl.get(x));
                    label_2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            deleteLabel(1);
                        }
                    });
                    break;
                case 2:
                    label_3.setVisibility(View.VISIBLE);
                    label_3.setText(lbl.get(x));
                    label_3.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteLabel(2);
                        }
                    });
                    break;
                case 3:
                    label_4.setVisibility(View.VISIBLE);
                    label_4.setText(lbl.get(x));
                    label_4.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteLabel(3);
                        }
                    });
                    break;
                case 4:
                    label_5.setVisibility(View.VISIBLE);
                    label_5.setText(lbl.get(x));
                    label_5.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteLabel(4);
                        }
                    });
                    break;
                case 5:
                    label_6.setVisibility(View.VISIBLE);
                    label_6.setText(lbl.get(x));
                    label_6.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteLabel(5);
                        }
                    });
                    break;
                case 6:
                    label_7.setVisibility(View.VISIBLE);
                    label_7.setText(lbl.get(x));
                    label_7.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteLabel(6);
                        }
                    });
                    break;
                case 7:
                    label_8.setVisibility(View.VISIBLE);
                    label_8.setText(lbl.get(x));
                    label_8.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteLabel(7);
                        }
                    });
                    break;
                case 8:
                    label_9.setVisibility(View.VISIBLE);
                    label_9.setText(lbl.get(x));
                    label_9.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteLabel(8);
                        }
                    });
                    break;
                case 9:
                    label_10.setVisibility(View.VISIBLE);
                    label_10.setText(lbl.get(x));
                    label_10.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteLabel(9);
                        }
                    });
                    break;
            }
            x++;
        }
    }

    void deleteLabel(int index){
        lbl.remove(index);
        setLabelGone();
        setLabelListener();
    }

    void setAssigneeGone(){
        btn_asg1.setVisibility(View.GONE);
        btn_asg2.setVisibility(View.GONE);
        btn_asg3.setVisibility(View.GONE);
        btn_asg4.setVisibility(View.GONE);
        btn_asg5.setVisibility(View.GONE);
        btn_asg6.setVisibility(View.GONE);
        btn_asg7.setVisibility(View.GONE);
        btn_asg8.setVisibility(View.GONE);
        btn_asg9.setVisibility(View.GONE);
        btn_asg10.setVisibility(View.GONE);
    }

    void setAssginees(){
        asg.clear();
        splitString(cutString(Task.assignees), "assignee");
        clearString("assignee");
        int i = 0;
        while (i < asg.size()){
            switch (i){
                case 0:
                    btn_asg1.setVisibility(View.VISIBLE);
                    btn_asg1.setText(asg.get(i));
                    btn_asg1.setBackgroundResource(R.drawable.back_assignees);
                    break;
                case 1:
                    btn_asg2.setVisibility(View.VISIBLE);
                    btn_asg2.setText(asg.get(i));
                    btn_asg2.setBackgroundResource(R.drawable.back_assignees);
                    break;
                case 2:
                    btn_asg3.setVisibility(View.VISIBLE);
                    btn_asg3.setText(asg.get(i));
                    btn_asg3.setBackgroundResource(R.drawable.back_assignees);
                    break;
                case 3:
                    btn_asg4.setVisibility(View.VISIBLE);
                    btn_asg4.setText(asg.get(i));
                    btn_asg4.setBackgroundResource(R.drawable.back_assignees);
                    break;
                case 4:
                    btn_asg5.setVisibility(View.VISIBLE);
                    btn_asg5.setText(asg.get(i));
                    btn_asg5.setBackgroundResource(R.drawable.back_assignees);
                    break;
                case 5:
                    btn_asg6.setVisibility(View.VISIBLE);
                    btn_asg6.setText(asg.get(i));
                    btn_asg6.setBackgroundResource(R.drawable.back_assignees);
                    break;
                case 6:
                    btn_asg7.setVisibility(View.VISIBLE);
                    btn_asg7.setText(asg.get(i));
                    btn_asg7.setBackgroundResource(R.drawable.back_assignees);
                    break;
                case 7:
                    btn_asg8.setVisibility(View.VISIBLE);
                    btn_asg8.setText(asg.get(i));
                    btn_asg8.setBackgroundResource(R.drawable.back_assignees);
                    break;
                case 8:
                    btn_asg9.setVisibility(View.VISIBLE);
                    btn_asg9.setText(asg.get(i));
                    btn_asg9.setBackgroundResource(R.drawable.back_assignees);
                    break;
                case 9:
                    btn_asg10.setVisibility(View.VISIBLE);
                    btn_asg10.setText(asg.get(i));
                    btn_asg10.setBackgroundResource(R.drawable.back_assignees);
                    break;
            }
            i++;
        }
    }

    void setAssigneeListener(){
//        asg = asg_cut;
        int x = 0;
        assignee_all = "";
        jumlah_asg = asg.size();

        while (x < asg.size()){
            if (assignee_all.equals("")){
                assignee_all = asg.get(x);
            } else {
                assignee_all = assignee_all + "," + asg.get(x);
            }
            switch (x){
                case 0:
                    btn_asg1.setVisibility(View.VISIBLE);
                    btn_asg1.setText(asg.get(x));
                    btn_asg1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteAssignee(0);
                        }
                    });
                    break;
                case 1:
                    btn_asg2.setVisibility(View.VISIBLE);
                    btn_asg2.setText(asg.get(x));
                    btn_asg2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteAssignee(1);
                        }
                    });
                    break;
                case 2:
                    btn_asg3.setVisibility(View.VISIBLE);
                    btn_asg3.setText(asg.get(x));
                    btn_asg3.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteAssignee(2);
                        }
                    });
                    break;
                case 3:
                    btn_asg4.setVisibility(View.VISIBLE);
                    btn_asg4.setText(asg.get(x));
                    btn_asg4.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteAssignee(3);
                        }
                    });
                    break;
                case 4:
                    btn_asg5.setVisibility(View.VISIBLE);
                    btn_asg5.setText(asg.get(x));
                    btn_asg5.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteAssignee(4);
                        }
                    });
                    break;
                case 5:
                    btn_asg6.setVisibility(View.VISIBLE);
                    btn_asg6.setText(asg.get(x));
                    btn_asg6.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteAssignee(5);
                        }
                    });
                    break;
                case 6:
                    btn_asg7.setVisibility(View.VISIBLE);
                    btn_asg7.setText(asg.get(x));
                    btn_asg7.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteAssignee(6);
                        }
                    });
                    break;
                case 7:
                    btn_asg8.setVisibility(View.VISIBLE);
                    btn_asg8.setText(asg.get(x));
                    btn_asg8.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteAssignee(7);
                        }
                    });
                    break;
                case 8:
                    btn_asg9.setVisibility(View.VISIBLE);
                    btn_asg9.setText(asg.get(x));
                    btn_asg9.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteAssignee(8);
                        }
                    });
                    break;
                case 9:
                    btn_asg10.setVisibility(View.VISIBLE);
                    btn_asg10.setText(asg.get(x));
                    btn_asg10.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteAssignee(9);
                        }
                    });
                    break;
            }
            x++;
        }
    }

    void deleteAssignee(int index){
        asg.remove(index);
        setAssigneeGone();
        setAssigneeListener();
    }

    // remove [...]
    public String cutString(String labels){
        String result = "";
        String tmp = "";
        int panjang = labels.length();
        tmp = labels.substring(1, (panjang-1));
        result = tmp;
        return result;
    }

    // split label
    public void splitString(String input, String jenis){
        if (jenis.equals("label")){
            for (String result: input.split(",")){
                lbl.add(result);
            }
        } else {
            for (String result: input.split(",")){
                asg.add(result);
            }
        }
    }

    // remove ""
    public void clearString(String jenis){
        if (jenis.equals("label")){
            int total = lbl.size();
            int i = 0;
            cek_cut = "";
            String tmp = "";
            while (i < total) {
                try {
                    tmp = lbl.get(i);
                    int panjang = tmp.length();
                    tmp = tmp.substring(1,(panjang-1));
                    lbl.set(i, tmp);
                    if (cek_cut.equals("")){
                        cek_cut = lbl.get(i);
                    } else {
                        cek_cut = cek_cut + " - " + lbl.get(i);
                    }
                } catch(Exception e){
                    e.printStackTrace();
                }
                i++;
            }
        } else {
            int total = asg.size();
            int i = 0;
            cek_cut = "";
            String tmp = "";
            while (i < total) {
                try {
                    tmp = asg.get(i);
                    int panjang = tmp.length();
                    tmp = tmp.substring(1,(panjang-1));
                    asg.set(i, tmp);
                    if (cek_cut.equals("")){
                        cek_cut = asg.get(i);
                    } else {
                        cek_cut = cek_cut + " - " + asg.get(i);
                    }
                } catch(Exception e){
                    e.printStackTrace();
                }
                i++;
            }
        }

    }

    void showToast(String msg){
        Toast.makeText(UpdateTask.this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = dayOfMonth+"-"+(++monthOfYear)+"-"+year;
        txt_date.setText(date);
    }
    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
        String hourString = hourOfDay < 10 ? "0"+hourOfDay : ""+hourOfDay;
        String minuteString = minute < 10 ? "0"+minute : ""+minute;
        String time = hourString+":"+minuteString;
        txt_time.setText(time);
    }

    @Override
    public void onResume() {
        super.onResume();
        TimePickerDialog tpd = (TimePickerDialog) getFragmentManager().findFragmentByTag("Timepickerdialog");
        DatePickerDialog dpd = (DatePickerDialog) getFragmentManager().findFragmentByTag("Datepickerdialog");

        if(tpd != null) tpd.setOnTimeSetListener(this);
        if(dpd != null) dpd.setOnDateSetListener(this);
    }

    private class GetEmployee extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(UpdateTask.this);
            pDialog.setMessage("Retrieve Data ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                URLConnection urlconnection = new URLConnection();
                employee_response = urlconnection.getMethod(Data.uriGetAllEmployee);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject obj = new JSONObject(employee_response);
                        JSONArray arr = (JSONArray) obj.getJSONArray("employee");
                        for (int i = 0; i < arr.length(); i++){
                            JSONObject row = arr.getJSONObject(i);
                            employee_list.add(row.getString("employee_name"));
                        }
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                    employee_adapter = new ArrayAdapter<String>(UpdateTask.this, android.R.layout.simple_list_item_1, employee_list);
                    txt_owner.setAdapter(employee_adapter);
                    txt_owner.setThreshold(1);
                    txt_assignee.setAdapter(employee_adapter);
                    txt_assignee.setThreshold(1);
                }
            });
        }
    }

    private class sendUpdate extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(UpdateTask.this);
            pDialog.setMessage("Sending Data ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                // Create parameters JSONObject
                JSONObject parameters = new JSONObject();
                parameters.put("id_task", id_task);
                parameters.put("title", title_task);
                parameters.put("workspace", workspace_task);
                parameters.put("description", description_task);
                parameters.put("owner", owner_task);
                parameters.put("estimation_time", estimation_task);
                parameters.put("spent_time", spent_task);
                parameters.put("due_date", due_task);
                parameters.put("labels", label_task);
                parameters.put("assignees", assignee_task);
                URLConnection urlconnection = new URLConnection();
                responsemsg = urlconnection.putMethod(Data.uriTask, parameters.toString());
                JSONObject row = new JSONObject(responsemsg);
                parseResponse = row.getString("msg");

            } catch (Exception e) {
                parseResponse = "" + e;
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showToast(parseResponse);
                    Intent i = new Intent(UpdateTask.this, MainActivity.class);
                    startActivity(i);
                    UpdateTask.this.finish();
                }
            });
        }
    }

}
