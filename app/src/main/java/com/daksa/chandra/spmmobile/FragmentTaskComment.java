package com.daksa.chandra.spmmobile;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.daksa.chandra.controller.Task;
import com.daksa.chandra.controller.URLConnection;
import com.daksa.chandra.data.Data;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by User on 9/14/2015.
 */
public class FragmentTaskComment extends Fragment {

    ListView listview;
    EditText txt_comment;
    ImageButton btn_comment;
    ProgressDialog pDialog;

    String responsemsg = "";
    String parseResponse = "";
    String comment = "";
    String l_comment = "";
    String l_user = "";
    String l_date = "";
    Handler h = new Handler();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Get the view from fragmenttab1.xml
        View view = inflater.inflate(R.layout.frag_comment, container, false);
        listview = (ListView) view.findViewById(R.id.list_comment_task);
        txt_comment = (EditText) view.findViewById(R.id.txt_comment_task);
        btn_comment = (ImageButton) view.findViewById(R.id.btn_comment_task);
        Data.fNumber = 1;
        return view;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        h.removeCallbacksAndMessages(null);
        btn_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comment = txt_comment.getText().toString();
                new PostComment().execute();
            }
        });
        try{
            new JSONParse().execute();
        } catch (Exception e){
            e.printStackTrace();
            showToast("Error : "+e);
        }
    }

    void showToast(String msg){
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    private class JSONParse extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... args) {
            URLConnection urlconn = new URLConnection();
            responsemsg = urlconn.getMethod(Data.uriComment+""+ Task.task_id);
            return null;

        }

        @Override
        protected void onPostExecute(String result) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject obj = new JSONObject(responsemsg);
                        JSONArray arr = (JSONArray) obj.getJSONArray("content");
                        ArrayList<HashMap<String, String>> commentlist = new ArrayList<>();
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject row = arr.getJSONObject(i);
                            l_user = row.getString("user");
                            l_comment = row.getString("comment");
                            l_date = row.getString("comment_date");

                            HashMap<String, String> map = new HashMap<>();
                            map.put("user", l_user);
                            map.put("comment", l_comment);
                            map.put("date", l_date);
                            commentlist.add(map);

                            ListAdapter adapter = new SimpleAdapter(getActivity(), commentlist, R.layout.list_comment,
                                    new String[]{"user", "comment", "date"},
                                    new int[]{R.id.txt_comment_task_name, R.id.txt_comment_task_isi, R.id.txt_comment_task_date});
                            listview.setAdapter(adapter);
                            listview.startLayoutAnimation();
                        }
                    } catch (Exception e) {
                        showToast("" + e);
                        Log.e("ERROR", "run ", e);
                    }
                }
            });
        }
    }

    private class PostComment extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Posting Comment ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                // Create parameters JSONObject
                JSONObject parameters = new JSONObject();
                parameters.put("id_task", Task.task_id);
                parameters.put("user", "Yuniar Pristyan Chandra");
                parameters.put("comment", comment);

                URLConnection urlconnection = new URLConnection();
                responsemsg = urlconnection.postMethod(Data.uriComment, parameters.toString());
                JSONObject row = new JSONObject(responsemsg);
                parseResponse = row.getString("msg");

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showToast(parseResponse);
                    txt_comment.setText("");
                }
            });
        }
    }
}
