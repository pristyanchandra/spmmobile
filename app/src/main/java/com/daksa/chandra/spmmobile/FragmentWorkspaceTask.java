package com.daksa.chandra.spmmobile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.daksa.chandra.controller.TaskAdapter;
import com.daksa.chandra.controller.URLConnection;
import com.daksa.chandra.controller.Workspace;
import com.daksa.chandra.data.Data;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by User on 9/23/2015.
 */
public class FragmentWorkspaceTask extends Fragment {

    Handler h = new Handler();
    ImageButton btn_tambah;
    ListView listview;
    public String content;
    public ProgressDialog pDialog;
    public String task_id = "";
    public String task_title = "";
    public String task_workspace = "";
    public String task_status = "";
    public String task_due = "";
    public String task_owner = "";
    public String task_description = "";
    public String task_created_time = "";
    public String task_last_modified = "";
    public String task_labels = "";
    public String task_spent_time = "";
    public String task_estimation_time = "";
    public String task_assignees = "";

    public ArrayList<String> t_id = new ArrayList<>();
    public ArrayList<String> t_title = new ArrayList<>();
    public ArrayList<String> t_workspace = new ArrayList<>();
    public ArrayList<String> t_due = new ArrayList<>();
    public ArrayList<String> t_spent = new ArrayList<>();
    public ArrayList<String> t_label = new ArrayList<>();
    public ArrayList<String> t_status = new ArrayList<>();
    public ArrayList<String> t_assignees = new ArrayList<>();
    public ArrayList<String> t_description= new ArrayList<>();
    public ArrayList<String> t_owner = new ArrayList<>();
    public ArrayList<String> t_createdtime = new ArrayList<>();
    public ArrayList<String> t_lastmodified = new ArrayList<>();
    public ArrayList<String> t_estimation = new ArrayList<>();

    LayoutAnimationController lac;
    SwipeRefreshLayout srl;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_workspace_task, container, false);
        listview = (ListView) view.findViewById(R.id.list_task);
        srl = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout_task);
        btn_tambah = (ImageButton) view.findViewById(R.id.btn_tambah_task);
        btn_tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), FragmentAdd.class);
                startActivity(intent);
            }
        });
        return view;
    }

    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        h.removeCallbacksAndMessages(null);
        srl.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        srl.setRefreshing(true);
                        new JSONParse().execute();
                    }
                });
        try{
            new JSONParse().execute();
        } catch (Exception e) {
            showToast(""+e);
        }
    }

    void showToast(String msg){
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    private class JSONParse extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Retrieving Data ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {
            URLConnection urlconn = new URLConnection();
            content = urlconn.getMethod(Data.uriTaskWorkspace + Workspace.id_static);
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject obj = new JSONObject(content);
                        JSONArray arr = (JSONArray) obj.getJSONArray(Data.TAG_TASK);
                        Data.task.clear();
                        t_id = new ArrayList<>();
                        t_title = new ArrayList<>();
                        t_workspace = new ArrayList<>();
                        t_due = new ArrayList<>();
                        t_spent = new ArrayList<>();
                        t_label = new ArrayList<>();
                        t_status = new ArrayList<>();
                        t_assignees = new ArrayList<>();
                        t_description= new ArrayList<>();
                        t_owner = new ArrayList<>();
                        t_createdtime = new ArrayList<>();
                        t_lastmodified = new ArrayList<>();
                        t_estimation = new ArrayList<>();
                        for (int i = 0; i < arr.length(); i++){
                            JSONObject row = arr.getJSONObject(i);

                            task_id = row.getString(Data.TAG_TASK_ID);
                            task_title = row.getString(Data.TAG_TASK_TITLE);
                            task_workspace = row.getString(Data.TAG_TASK_WORKSPACE);
                            task_due = row.getString(Data.TAG_TASK_DUE);
                            task_status = row.getString(Data.TAG_TASK_STATUS);
                            task_owner = row.getString(Data.TAG_TASK_OWNER);
                            task_last_modified = row.getString(Data.TAG_TASK_MODIFIED);
                            task_created_time = row.getString(Data.TAG_TASK_CREATED);
                            task_description = row.getString(Data.TAG_TASK_DESCRIPTION);
                            task_spent_time = row.getString(Data.TAG_TASK_SPENT);
                            task_labels = row.getString(Data.TAG_TASK_LABELS);
                            task_estimation_time = row.getString(Data.TAG_TASK_ESTIMATION);
                            task_assignees = row.getString(Data.TAG_TASK_ASSIGNEES);

                            t_id.add(task_id);
                            t_title.add(task_title);
                            t_workspace.add(task_workspace);
                            t_due.add(task_due);
                            t_status.add(task_status);
                            t_spent.add(task_spent_time);
                            t_label.add(task_labels);
                            t_description.add(task_description);
                            t_owner.add(task_owner);
                            t_estimation.add(task_estimation_time);
                            t_createdtime.add(task_created_time);
                            t_lastmodified.add(task_last_modified);
                            t_assignees.add(task_assignees);

                            try {
                                listview.setAdapter(new TaskAdapter(getActivity() ,t_id, t_title, t_workspace, t_due, t_spent, t_label, t_status,
                                        t_assignees, t_description, t_createdtime, t_lastmodified, t_estimation, t_owner));
                                lac = new LayoutAnimationController(AnimationUtils.loadAnimation(getActivity(), R.anim.zoom_in), 0.5f); //0.5f == time between appearance of listview items.
                                listview.setLayoutAnimation(lac);
                                listview.startLayoutAnimation();
                                srl.setRefreshing(false);

                            } catch (Exception e) {
                                showToast(t_id.get(i) + " | " + t_title.get(i) + " | " + t_workspace.get(i) + " | " + t_due.get(i) + " | " + t_spent.get(i) + " | " + t_label.get(i) + " | " + t_status.get(i)
                                        + " | " + t_assignees.get(i) + " | " + t_description.get(i) + " | " + t_createdtime.get(i) + " | " + t_lastmodified.get(i) + " | " + t_estimation.get(i) + " | " + t_owner.get(i));
//                                showToast(""+e);
                                e.printStackTrace();
                            }
                        }
//                        showToast(content);
                    } catch (Exception e) {
                        showToast(content);
                        showToast(""+e);
                        Log.e("ERROR", "run ", e);
                    }
                }
            });
        }
    }

}
