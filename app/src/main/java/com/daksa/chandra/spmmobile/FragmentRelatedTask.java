package com.daksa.chandra.spmmobile;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.daksa.chandra.controller.URLConnection;
import com.daksa.chandra.data.Data;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 9/21/2015.
 */
public class FragmentRelatedTask extends Fragment {

    Handler h = new Handler();
    AppCompatAutoCompleteTextView txt_related;
    ImageButton btn_related;
    ListView listview;

    String related_response;
    ArrayAdapter<String> related_adapter;
    List<String> related_list = new ArrayList<String>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_relatedtask, container, false);
        txt_related = (AppCompatAutoCompleteTextView) view.findViewById(R.id.txt_addtask_related);
        listview = (ListView) view.findViewById(R.id.lv_related);
        btn_related = (ImageButton) view.findViewById(R.id.btn_addtask_related);
        Data.fNumber = 1;
        return view;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        h.removeCallbacksAndMessages(null);
        new GetRelated().execute();
        setListview();
        btn_related.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Data.list_related.add(txt_related.getText().toString());
                setListview();
                txt_related.setText("");
            }
        });
    }

    void setListview(){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.list_related, R.id.txt_related, Data.list_related);
        listview.setAdapter(adapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                removeList(position);
            }
        });

    }

    void removeList(int position){
        Data.list_related.remove(position);
        setListview();
    }

    void showToast(String msg){
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    private class GetRelated extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                URLConnection urlconnection = new URLConnection();
                related_response = urlconnection.getMethod(Data.uriTaskWorkspace+"1");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject obj = new JSONObject(related_response);
                        JSONArray arr = (JSONArray) obj.getJSONArray("task");
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject row = arr.getJSONObject(i);
                            related_list.add(row.getString("title"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    related_adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, related_list);
                    txt_related.setAdapter(related_adapter);
                    txt_related.setThreshold(1);
                }
            });
        }
    }

}
