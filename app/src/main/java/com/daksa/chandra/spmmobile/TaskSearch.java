package com.daksa.chandra.spmmobile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.Spinner;

import com.daksa.chandra.controller.DateConversion;
import com.daksa.chandra.controller.ExpandableAdapter;
import com.daksa.chandra.controller.URLConnection;
import com.daksa.chandra.data.Data;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by User on 9/16/2015.
 */
public class TaskSearch extends ActionBarActivity implements
        TimePickerDialog.OnTimeSetListener,
        DatePickerDialog.OnDateSetListener {
    private Toolbar topToolBar;
    Spinner spn_to;
    EditText txt_title;
    TextView txt_createddate_from;
    TextView txt_createdtime_from;
    TextView txt_createddate_until;
    TextView txt_createdtime_until;
    TextView txt_duedate_from;
    TextView txt_duetime_from;
    TextView txt_duedate_until;
    TextView txt_duetime_until;
    Button btn_search;
    ProgressDialog pDialog;

    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;

    ArrayAdapter<CharSequence> adapter;
    String assignees_response = "";
    String label_response = "";

    String tmp = "";

    String title = "";
    String labels = "";
    String assignee = "";
    String status = "";
    String to = "";
    String createddate_from = "";
    String createddate_until = "";
    String createdtime_from = "";
    String createdtime_until = "";
    String duedate_from = "";
    String duedate_until = "";
    String duetime_from = "";
    String duetime_until = "";
    String due_from = "";
    String due_until = "";
    String created_from = "";
    String created_until = "";

    String responsemsg = "";
    String parseResponse = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.task_search);
        topToolBar = (Toolbar) findViewById(R.id.toolbar_searchtask);
        setSupportActionBar(topToolBar);
        getSupportActionBar().setTitle(R.string.search_task);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        Data.clearSearch();

        txt_title = (EditText) findViewById(R.id.txt_search_title);
        spn_to = (Spinner) findViewById(R.id.spn_search_to);
        adapter = ArrayAdapter.createFromResource(TaskSearch.this, R.array.adapter_to, android.R.layout.simple_spinner_dropdown_item);
        spn_to.setAdapter(adapter);

        expListView = (ExpandableListView) findViewById(R.id.xpn_option);
        fillData();
        listAdapter = new ExpandableAdapter(TaskSearch.this, listDataHeader, listDataChild);
        expListView.setAdapter(listAdapter);

        txt_createddate_from = (TextView) findViewById(R.id.txt_search_createddate_from);
        txt_createddate_from.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        TaskSearch.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                tmp = "createdfrom";
                dpd.setAccentColor(Color.parseColor("#00BCD4"));
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });

        txt_createdtime_from = (TextView) findViewById(R.id.txt_search_createdtime_from);
        txt_createdtime_from.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                TimePickerDialog tpd = TimePickerDialog.newInstance(TaskSearch.this, now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), true);
                tpd.setAccentColor(Color.parseColor("#00BCD4"));
                tpd.show(getFragmentManager(), "Timepickerdialog");
                tmp = "createdfrom";
            }
        });

        txt_createddate_until = (TextView) findViewById(R.id.txt_search_createddate_until);
        txt_createddate_until.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        TaskSearch.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                tmp = "createduntil";
                dpd.setAccentColor(Color.parseColor("#00BCD4"));
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });

        txt_createdtime_until = (TextView) findViewById(R.id.txt_search_createdtime_until);
        txt_createdtime_until.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                TimePickerDialog tpd = TimePickerDialog.newInstance(TaskSearch.this,now.get(Calendar.HOUR_OF_DAY),now.get(Calendar.MINUTE),true);
                tpd.setAccentColor(Color.parseColor("#00BCD4"));
                tpd.show(getFragmentManager(), "Timepickerdialog");
                tmp = "createduntil";
            }
        });

        txt_duedate_from = (TextView) findViewById(R.id.txt_search_duedate_from);
        txt_duedate_from.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        TaskSearch.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                tmp = "duefrom";
                dpd.setAccentColor(Color.parseColor("#00BCD4"));
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });

        txt_duetime_from = (TextView) findViewById(R.id.txt_search_duetime_from);
        txt_duetime_from.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                TimePickerDialog tpd = TimePickerDialog.newInstance(TaskSearch.this,now.get(Calendar.HOUR_OF_DAY),now.get(Calendar.MINUTE),true);
                tpd.setAccentColor(Color.parseColor("#00BCD4"));
                tpd.show(getFragmentManager(), "Timepickerdialog");
                tmp = "duefrom";
            }
        });

        txt_duedate_until = (TextView) findViewById(R.id.txt_search_duedate_until);
        txt_duedate_until.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        TaskSearch.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                tmp = "dueuntil";
                dpd.setAccentColor(Color.parseColor("#00BCD4"));
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });

        txt_duetime_until = (TextView) findViewById(R.id.txt_search_duetime_until);
        txt_duetime_until.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                TimePickerDialog tpd = TimePickerDialog.newInstance(TaskSearch.this,now.get(Calendar.HOUR_OF_DAY),now.get(Calendar.MINUTE),true);
                tpd.setAccentColor(Color.parseColor("#00BCD4"));
                tpd.show(getFragmentManager(), "Timepickerdialog");
                tmp = "dueuntil";
            }
        });

        btn_search = (Button) findViewById(R.id.btn_search_task);
        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                title = txt_title.getText().toString();
                to = spn_to.getSelectedItem().toString();

                createddate_from = txt_createddate_from.getText().toString();
                createddate_until = txt_createddate_until.getText().toString();
                createdtime_from = txt_createdtime_from.getText().toString();
                createdtime_until = txt_createdtime_until.getText().toString();
                duedate_from = txt_duedate_from.getText().toString();
                duedate_until = txt_duedate_until.getText().toString();
                duetime_from = txt_duetime_from.getText().toString();
                duetime_until = txt_duetime_until.getText().toString();

                if (createddate_from.equals("Add Date") || createdtime_from.equals("Add Time")) {
                    createddate_from = "";
                    createdtime_from = "";
                    created_from = "";
                } else {
                    created_from = createddate_from + " " + createdtime_from;
                }

                if (createddate_until.equals("Add Date") || createdtime_until.equals("Add Time")){
                    createddate_until = "";
                    createdtime_until = "";
                    created_until = "";
                } else {
                    created_until = createddate_until + " " + createdtime_until;
                }

                if (duedate_from.equals("Add Date") || duetime_from.equals("Add Time")) {
                    duedate_from = "";
                    duetime_from = "";
                    due_from = "";
                } else {
                    due_from = duedate_from + " " + duetime_from;
                }

                if (duedate_until.equals("Add Date") || duetime_until.equals("Add Time")){
                    duedate_until = "";
                    duetime_until = "";
                    due_until = "";
                } else {
                    due_until = duedate_until + " " + duetime_until;
                }

                labels = Data.srcLabels;
                assignee = Data.srcAssignees;
                status = Data.srcStatus;

                new SearchTask().execute();
            }
        });
    }

    void fillData(){
        listDataChild = new HashMap<String, List<String>>();
        listDataHeader = new ArrayList<String>();

        listDataHeader.add("Labels");
        listDataHeader.add("Assignees");
        listDataHeader.add("Status");

        new GetLabels().execute();
        new GetAssignees().execute();

        List<String> status = new ArrayList<>();
        status.add("Completed");
        status.add("In Progress");
        status.add("New");
        status.add("Rejected");
        status.add("Reviewed");

        listDataChild.put(listDataHeader.get(2), status);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = dayOfMonth+"-"+(++monthOfYear)+"-"+year;
        DateConversion dc = new DateConversion();

        if (tmp.equals("createdfrom")){
            txt_createddate_from.setText(dc.conv(date));
        } else if (tmp.equals("createduntil")){
            txt_createddate_until.setText(dc.conv(date));
        } else if (tmp.equals("duefrom")){
            txt_duedate_from.setText(dc.conv(date));
        } else if (tmp.equals("dueuntil")) {
            txt_duedate_until.setText(dc.conv(date));
        }
    }
    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
        String hourString = hourOfDay < 10 ? "0"+hourOfDay : ""+hourOfDay;
        String minuteString = minute < 10 ? "0"+minute : ""+minute;
        String time = hourString+":"+minuteString;
        if (tmp.equals("createdfrom")){
            txt_createdtime_from.setText(time);
        } else if (tmp.equals("createduntil")){
            txt_createdtime_until.setText(time);
        } else if (tmp.equals("duefrom")){
            txt_duetime_from.setText(time);
        } else if (tmp.equals("dueuntil")) {
            txt_duetime_until.setText(time);
        }
    }

    private class GetAssignees extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                URLConnection urlconnection = new URLConnection();
                assignees_response = urlconnection.getMethod(Data.uriGetAssignees + "1");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject obj = new JSONObject(assignees_response);
                        JSONArray arr = (JSONArray) obj.getJSONArray("employee");
                        List<String> assignees = new ArrayList<String>();
                        for (int i = 0; i < arr.length(); i++){
                            JSONObject row = arr.getJSONObject(i);
                            assignees.add(row.getString("employee_name"));
                        }
                        listDataChild.put(listDataHeader.get(1), assignees);
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    void showToast(String msg){
        Toast.makeText(TaskSearch.this, msg, Toast.LENGTH_LONG).show();
    }

    private class GetLabels extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                URLConnection urlconnection = new URLConnection();
                label_response = urlconnection.getMethod(Data.uriLabelWorkspace + "1");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject obj = new JSONObject(label_response);
                        JSONArray arr = (JSONArray) obj.getJSONArray("labels");
                        List<String> assignees = new ArrayList<String>();
                        for (int i = 0; i < arr.length(); i++){
                            JSONObject row = arr.getJSONObject(i);
                            assignees.add(row.getString("label"));
                        }
                        listDataChild.put(listDataHeader.get(0), assignees);
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private class SearchTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(TaskSearch.this);
            pDialog.setMessage("Searching Data ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                // Create parameters JSONObject
                JSONObject parameters = new JSONObject();
                parameters.put("title", title);
                parameters.put("to", to);
                parameters.put("labels", labels);
                parameters.put("assignees", assignee);
                parameters.put("status", status);
                parameters.put("created_from", created_from);
                parameters.put("created_until", created_until);
                parameters.put("due_from", due_from);
                parameters.put("due_until", due_until);
//                tmp = parameters.toString();
                URLConnection urlconnection = new URLConnection();
                responsemsg = urlconnection.postMethod(Data.uriSearchTask, parameters.toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
//                    showToast(tmp);
//                    showToast(responsemsg);
                    Data.searchResult = responsemsg;
                    Intent i = new Intent(TaskSearch.this, TaskSearchResult.class);
                    startActivity(i);
                }
            });
        }
    }

}
